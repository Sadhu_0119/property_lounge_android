package com.techgropse.propertylounge.otp

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.model.ResponseForgotPass
import com.techgropse.propertylounge.model.ResponseOTPValidate
import com.techgropse.propertylounge.resetpass.ResetPasswordActivity
import com.techgropse.propertylounge.utils.*
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.dialog_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit

class OTPVerificationActivity : BaseActivity() {
    var scr=""
    var userId=""
    var otpuserRequest:Call<ResponseOTPValidate>?=null
    var mPref:SharedPreferenceUtils?=null
    var isPaused = true
    var resendOtpRequest:Call<ResponseForgotPass>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        iniT()
        iniTcontrol()
    }

    private fun iniT() {
        mPref= SharedPreferenceUtils(this)
        scr=intent.getStringExtra("scr")!!
        userId=intent.getStringExtra("userId")!!

        val timer = object : CountDownTimer(60000, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                tvResend.isEnabled=false
                tvTimer.visibility=View.VISIBLE
                tvTimer.text = timeString(millisUntilFinished)+" sec"
            }

            override fun onFinish() {
                if (isPaused) {
                    tvResend.isEnabled=true
                    tvTimer.visibility=View.GONE
                }
            }
        }
        timer.start()
        setUpOtpProp()
    }

    // Method to get days hours minutes seconds from milliseconds
    private fun timeString(millisUntilFinished: Long): String {
        val millisUntilFinished: Long = millisUntilFinished

        val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
        // Format the string
        return String.format(
            Locale.getDefault(), "00:%02d", seconds
        )
    }

    private fun iniTcontrol() {
        btnSubmit.setOnClickListener {

            if (!isValidOtp()){
                Toast.makeText(this,resources.getString(R.string.hint_enter_otp), Toast.LENGTH_SHORT).show()
            } else{
                ///api call to verify OTP
                ProgressDialogUtils.getInstance().showProgress(this,false)
                APIforOTPVerification(userId,getOtp())
            }
        }

        tvResend.setOnClickListener {
            iniT()
            ///api to resend otp
            ApiToResendOTP(userId)
        }

        backblack.setOnClickListener {
            onBackPressed()
        }
    }

    ///api call to verify OTP
    private fun APIforOTPVerification(userId: String, otp: String) {
        otpuserRequest = ApiClient.apiClient!!.create(Apis::class.java).verifyOTPUser("1","123",userId,otp,"123")
        otpuserRequest!!.enqueue(object : Callback<ResponseOTPValidate?>
        {
            override fun onResponse(
                call: Call<ResponseOTPValidate?>,
                response: Response<ResponseOTPValidate?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        mPref!!.putString(Constants.EMAIL,response.body()!!.data.user_login_info.email)
                        mPref!!.putString(Constants.MOBILE,response.body()!!.data.user_login_info.mobile)
                        mPref!!.putString(Constants.NAME,response.body()!!.data.user_login_info.full_name)
                        mPref!!.putString(Constants.SECURITY_TOKEN,response.body()!!.data.user_login_info.security_token)
                        mPref!!.putString(Constants.DEVICE_ID,response.body()!!.data.user_login_info.device_id)
                        mPref!!.putString(Constants.USERID,response.body()!!.data.user_login_info.user_id)
                        mPref!!.putString(Constants.IMAGE,response.body()!!.data.user_login_info.image)

                        if (scr=="register"){
                            mPref!!.putBoolean(Constants.IS_LOGGED_IN,true)
                            startActivity(Intent(this@OTPVerificationActivity, HomeActivity::class.java))
                        }
                        else{
                            val intent = Intent(this@OTPVerificationActivity, ResetPasswordActivity::class.java)
                            intent.putExtra("userId",userId)
                            startActivity(intent)
                        }
                    }
                    else{
                        Toast.makeText(this@OTPVerificationActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            override fun onFailure(call: Call<ResponseOTPValidate?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@OTPVerificationActivity,t.message,Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setUpOtpProp() {

        etOtp1.addTextChangedListener(object : PropertyLoungeTextInputLayout {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (s.isNotEmpty()){
                        etOtp2.requestFocus()
                    }
                }
            }
        })

        etOtp2.addTextChangedListener(object : PropertyLoungeTextInputLayout {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (s.isNotEmpty()) etOtp3.requestFocus()
                }
            }
        })

        etOtp3.addTextChangedListener(object : PropertyLoungeTextInputLayout {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (s.isNotEmpty()) etOtp4.requestFocus()
                }
            }
        })

        etOtp4.addTextChangedListener(object : PropertyLoungeTextInputLayout {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    if (s.isNotEmpty()) hideKeyboard()
                }
            }
        })

        etOtp4.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    etOtp3.requestFocus()
                    etOtp3.setSelection(etOtp3.text!!.length)
                }
            }
            return@setOnKeyListener false
        }

        etOtp3.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    etOtp2.requestFocus()
                    etOtp2.setSelection(etOtp2.text!!.length)
                }
            }
            return@setOnKeyListener false
        }

        etOtp2.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    etOtp1.requestFocus()
                    etOtp1.setSelection(etOtp1.text!!.length)
                }
            }
            return@setOnKeyListener false
        }
    }

    //api integrate for resend otp api
    private fun ApiToResendOTP(userid: String) {
        resendOtpRequest = ApiClient.apiClient!!.create(Apis::class.java).resendOTp(userid)
        resendOtpRequest!!.enqueue(object : Callback<ResponseForgotPass?> {
            override fun onResponse(
                call: Call<ResponseForgotPass?>,
                response: Response<ResponseForgotPass?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        dialogopen(response.body()!!.data.user_login_info.otp.toString())
                    }
                    else{
                        Toast.makeText(this@OTPVerificationActivity, response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            override fun onFailure(call: Call<ResponseForgotPass?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@OTPVerificationActivity,t.message,Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen(otp: String) {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_otp)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvOtp.text=otp
        dialog.tvOk.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })
        dialog.show()
    }

    private fun isValidOtp(): Boolean {
        val otp = StringBuilder()
            .append(etOtp1.text.toString())
            .append(etOtp2.text.toString())
            .append(etOtp3.text.toString())
            .append(etOtp4.text.toString()).toString()
        return otp.isValidOtp()
    }

    private fun getOtp(): String {
        return StringBuilder()
            .append(etOtp1.text.toString())
            .append(etOtp2.text.toString())
            .append(etOtp3.text.toString())
            .append(etOtp4.text.toString()).toString()
    }

    override fun onPause() {
        super.onPause()
        isPaused = false
    }
    override fun onStop() {
        super.onStop()
        isPaused=false
    }


}