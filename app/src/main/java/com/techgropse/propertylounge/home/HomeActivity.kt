package com.techgropse.propertylounge.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.widget.Toast
import com.google.android.gms.location.LocationRequest
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterAgentList
import com.techgropse.propertylounge.adapter.Adapterproperty
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.login_sinup.LoginActivity
import com.techgropse.propertylounge.model.ResponseAgentList
import com.techgropse.propertylounge.model.ResponseHomeProperty
import com.techgropse.propertylounge.new_career.CareerActivity
import com.techgropse.propertylounge.new_career.NewsActivity
import com.techgropse.propertylounge.profile.ProfileActivity
import com.techgropse.propertylounge.property.PropertyAgentListActivity
import com.techgropse.propertylounge.property.SearchPropertyActivity
import com.techgropse.propertylounge.publish.PublishWithActivity
import com.techgropse.propertylounge.search.JoinUsActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.logout_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : BaseActivity() {
    var mPref: SharedPreferenceUtils?=null
    var userId=""
    var deviceId=""
    var securityToken=""
    var agentRequest:Call<ResponseAgentList>?=null
    var context: Context?=null
    var propertyRequest:Call<ResponseHomeProperty>?=null
    var facilitiesList:ArrayList<ResponseHomeProperty.Data.Feature>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        iniT()
        iniTcontrol()

        //Get Property List
        ProgressDialogUtils.getInstance().showProgress(this@HomeActivity,false)
        GetPropertyList()

    }


    private fun iniT() {
        context=this
        mPref= SharedPreferenceUtils(this)
        userId=mPref!!.getString(Constants.USERID,"")
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

        if (mPref!!.getBoolean(Constants.IS_LOGGED_IN, false)) {
            tvName.text=mPref!!.getString(Constants.NAME,"")
            if (!mPref!!.getString(Constants.IMAGE,"").isNullOrEmpty()) {
                Picasso.with(this).load(mPref!!.getString(Constants.IMAGE,"")).into(ivPerson)
            }
            else{
                Picasso.with(this).load(R.drawable.dummyplaceholder).into(ivPerson)
            }
        }
        else{
            tvName.text="Welcome Guest"
            tvviwall.text="Please login"
            tvlogin.text="Login"
        }


    }

    ///api  to get agent list
    private fun GetAgentListAPI() {
        agentRequest = ApiClient.apiClient!!.create(Apis::class.java).getAgentList()
        agentRequest!!.enqueue(object : Callback<ResponseAgentList?> {
            override fun onResponse(
                call: Call<ResponseAgentList?>,
                response: Response<ResponseAgentList?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    val list=response.body()!!.data.agent_list
                    val adapter=AdapterAgentList(list,this@HomeActivity)
                    rvAgentlist.adapter=adapter
                }

            }
            override fun onFailure(call: Call<ResponseAgentList?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@HomeActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    //Get Property List API
    private fun GetPropertyList() {
        propertyRequest = ApiClient.apiClient!!.create(Apis::class.java).propertyList()
        propertyRequest!!.enqueue(object : Callback<ResponseHomeProperty?> {
            override fun onResponse(
                call: Call<ResponseHomeProperty?>,
                response: Response<ResponseHomeProperty?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    val list=response.body()!!.data
                    val adapter1=Adapterproperty(context!!,list,facilitiesList)
                    rvPropertyList.adapter=adapter1

                    ///api  to get agent list
                    ProgressDialogUtils.getInstance().showProgress(this@HomeActivity,false)
                    GetAgentListAPI()

                }

            }
            override fun onFailure(call: Call<ResponseHomeProperty?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@HomeActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    @SuppressLint("WrongConstant")
    private fun iniTcontrol() {
        ivSideMenu.setOnClickListener {
            drawer.openDrawer(Gravity.START)
        }

        llprofile.setOnClickListener {
            if (mPref!!.getBoolean(Constants.IS_LOGGED_IN, false)) {
                startActivity(Intent(this,ProfileActivity::class.java))
            }
            else{
                startActivity(Intent(this,LoginActivity::class.java))
                finish()
            }
        }

        rlViewall.setOnClickListener {
            startActivity(Intent(this,PropertyAgentListActivity::class.java))
        }

        linAgents.setOnClickListener {
            startActivity(Intent(this,PropertyAgentListActivity::class.java))
        }
        search1.setOnClickListener {
            startActivity(Intent(this,SearchPropertyActivity::class.java))
        }

        linSearchproperty.setOnClickListener {
            startActivity(Intent(this,SearchPropertyActivity::class.java))

        }
        linLogout.setOnClickListener {
            if (mPref!!.getBoolean(Constants.IS_LOGGED_IN, false)) {
                //logout
                openDialog()
            }
            else{
                startActivity(Intent(this,LoginActivity::class.java))
                finish()
            }
        }

        linJoinus.setOnClickListener {
            startActivity(Intent(this,JoinUsActivity::class.java))
        }

        linNews.setOnClickListener {
            startActivity(Intent(this,NewsActivity::class.java))
        }

        linCareer.setOnClickListener {
            startActivity(Intent(this,CareerActivity::class.java))
        }

        linAbout.setOnClickListener {
            startActivity(Intent(this,ActivityAboutUs::class.java))
        }

        linTerms.setOnClickListener {
            startActivity(Intent(this,ActivityTermsNconditions::class.java))
        }

        linPrivacy.setOnClickListener {
            startActivity(Intent(this,ActivityPrivacyPolicy::class.java))
        }

        linPublish.setOnClickListener {
            startActivity(Intent(this,PublishWithActivity::class.java))
        }
    }

    var doubleBackToExitPressedOnce = false


    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click Back again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)

    }

    private fun openDialog() {
        val cutomSelectProfilePicBottomSheetDialog  =   BottomSheetDialog(this,R.style.SheetDialog)
        cutomSelectProfilePicBottomSheetDialog.setContentView(R.layout.logout_dialog)
        cutomSelectProfilePicBottomSheetDialog.show()
        cutomSelectProfilePicBottomSheetDialog.setCancelable(true)

        cutomSelectProfilePicBottomSheetDialog.tvYes.setOnClickListener {
             mPref!!.clear()
             mPref!!.putBoolean(Constants.IS_LOGGED_IN, false)
             startActivity(Intent(this, LoginActivity::class.java))
             finish()
        }

        cutomSelectProfilePicBottomSheetDialog.tvNo.setOnClickListener {
            cutomSelectProfilePicBottomSheetDialog.dismiss()
        }

        cutomSelectProfilePicBottomSheetDialog.close.setOnClickListener {
            cutomSelectProfilePicBottomSheetDialog.dismiss()
        }

    }

}