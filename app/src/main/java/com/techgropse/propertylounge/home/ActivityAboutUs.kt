package com.techgropse.propertylounge.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.webkit.WebSettings
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import kotlinx.android.synthetic.main.activity_about_us.*

class ActivityAboutUs: BaseActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        val webSettings: WebSettings = webView.settings
        webSettings.javaScriptEnabled = true

        ProgressDialogUtils.getInstance().showProgress(this, false)
        Handler().postDelayed({
            iniT()
        }, 8000)

        ivBackk.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        ProgressDialogUtils.getInstance().hideProgress()
        webView.loadUrl("https://www.goinstablog.com/goinstablog.com/sumitdesign/design/propertylounge.com/about-us")
    }
}