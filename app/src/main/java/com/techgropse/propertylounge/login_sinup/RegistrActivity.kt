package com.techgropse.propertylounge.login_sinup

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.home.ActivityTermsNconditions
import com.techgropse.propertylounge.model.ResponseRegistration
import com.techgropse.propertylounge.otp.OTPVerificationActivity
import com.techgropse.propertylounge.utils.*
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import kotlinx.android.synthetic.main.activity_registr.*
import kotlinx.android.synthetic.main.dialog_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistrActivity : BaseActivity() {
    var registerRequest: Call<ResponseRegistration>?=null
    var flag=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registr)

         edMobile.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(cs:CharSequence, arg1:Int, arg2:Int, arg3:Int) {
                // When user changed the Text
                val mobile=edMobile.text.toString().trim()
                flag = cs.toString()!=""
            }
            override fun beforeTextChanged(arg0:CharSequence, arg1:Int, arg2:Int,
                                           arg3:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(arg0: Editable) {
                // TODO Auto-generated method stub
            }
        })
        initControl()
    }

    private fun initControl() {
        btnSubmit.setOnClickListener {
            val name=edName.text.toString().trim()
            val mobile=edMobile.text.toString().trim()
            val email=edEmail.text.toString().trim()
            val password=edPassword.text.toString().trim()
            if (name.isEmpty()){
                Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_name), Toast.LENGTH_SHORT).show()
            } else if (email.isEmpty()){
                Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_email), Toast.LENGTH_SHORT).show()
            } else if (!email.isValidEmail()){
                Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_validemail), Toast.LENGTH_SHORT).show()
            }  else  if (password.isEmpty()) {
                Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_password), Toast.LENGTH_SHORT).show()
            } else if (!password.isValidPassword()) {
                Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }
            else{
                if (flag){
                    if (!mobile.isValidMobile){
                        Toast.makeText(this@RegistrActivity,resources.getString(R.string.error_validmobile), Toast.LENGTH_SHORT).show()
                    }
                    else{
                        //api call for registration
                        ProgressDialogUtils.getInstance().showProgress(this,false)
                        registerAPI(name,mobile,email,password)
                    }
                }
                else{
                    //api call for registration
                    ProgressDialogUtils.getInstance().showProgress(this,false)
                    registerAPI(name,mobile,email,password)
                }

            }
        }

        backblack.setOnClickListener {
            onBackPressed()
        }

        tvTerms.setOnClickListener {
            val intent=Intent(this, ActivityTermsNconditions::class.java)
            startActivity(intent)

        }
    }

    //api call to register into the app
    fun registerAPI(
        name: String,
        mobile: String,
        email: String,
        password: String
    ) {
        registerRequest = ApiClient.apiClient!!.create(Apis::class.java).registerUser(password,mobile,email,name,ccp.selectedCountryCodeWithPlus)
        registerRequest!!.enqueue(object : Callback<ResponseRegistration?> {
            override fun onResponse(
                call: Call<ResponseRegistration?>,
                response: Response<ResponseRegistration?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        dialogopen(response.body()!!.data.otp.toString(),response.body()!!.data.user_id)
                    }
                    else{
                        Toast.makeText(this@RegistrActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            override fun onFailure(call: Call<ResponseRegistration?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@RegistrActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen(otp: String, userId: String) {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_otp)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvOtp.text=otp
        dialog.tvOk.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            val intent=Intent(this, OTPVerificationActivity::class.java)
            intent.putExtra("scr","register")
            intent.putExtra("userId",userId)
            startActivity(intent)
        })
        dialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}