package com.techgropse.propertylounge.login_sinup

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.forgotpass.ForgotPasswordActivity
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.model.ResponseOTPValidate
import com.techgropse.propertylounge.otp.OTPVerificationActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.utils.isValidEmail
import com.techgropse.propertylounge.utils.isValidPassword
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import com.techgropse.propertylounge.welcomescreen.WelcomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.dialog_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {
    var loginuserRequest:Call<ResponseOTPValidate>?=null
    var mPref:SharedPreferenceUtils?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mPref= SharedPreferenceUtils(this)
        initControl()
    }

    private fun initControl() {
        tvForgot.setOnClickListener {
            startActivity(Intent(this,ForgotPasswordActivity::class.java))
        }

        btnSubmit.setOnClickListener {
            val email=edemail.text.toString().trim()
            val password=edpassword.text.toString().trim()

            if (email.isEmpty()){
                Toast.makeText(this@LoginActivity,resources.getString(R.string.error_email), Toast.LENGTH_SHORT).show()
            } else if (!email.isValidEmail()){
                Toast.makeText(this@LoginActivity,resources.getString(R.string.error_validemail), Toast.LENGTH_SHORT).show()
            }else  if (password.isEmpty()) {
                Toast.makeText(this@LoginActivity,resources.getString(R.string.error_password), Toast.LENGTH_SHORT).show()
            } else if (!password.isValidPassword()) {
                Toast.makeText(this@LoginActivity,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }
            else{
                //api to call login user
                ProgressDialogUtils.getInstance().showProgress(this,false)
                APIToCallUserLogin(email,password)
            }
        }

        backblack.setOnClickListener {
            onBackPressed()
        }
    }


    ///api to call user login
    private fun APIToCallUserLogin(email: String, password: String) {
        loginuserRequest = ApiClient.apiClient!!.create(Apis::class.java).loginUser("1","123",email,password,"123")
        loginuserRequest!!.enqueue(object : Callback<ResponseOTPValidate?> {
            override fun onResponse(
                call: Call<ResponseOTPValidate?>,
                response: Response<ResponseOTPValidate?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        mPref!!.putBoolean(Constants.IS_LOGGED_IN,true)
                        mPref!!.putString(Constants.EMAIL,response.body()!!.data.user_login_info.email)
                        mPref!!.putString(Constants.MOBILE,response.body()!!.data.user_login_info.mobile)
                        mPref!!.putString(Constants.NAME,response.body()!!.data.user_login_info.full_name)
                        mPref!!.putString(Constants.SECURITY_TOKEN,response.body()!!.data.user_login_info.security_token)
                        mPref!!.putString(Constants.DEVICE_ID,response.body()!!.data.user_login_info.device_id)
                        mPref!!.putString(Constants.USERID,response.body()!!.data.user_login_info.user_id)
                        mPref!!.putString(Constants.IMAGE,response.body()!!.data.user_login_info.image)

                        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                        finishAffinity()
                    }
                    else if (response.body()!!.error_code==204){
                        dialogopen(response.body()!!.data.user_login_info.otp,response.body()!!.data.user_login_info.user_id)
                    }
                    else{
                        Toast.makeText(this@LoginActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            override fun onFailure(call: Call<ResponseOTPValidate?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@LoginActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen(otp: Int, userId: String) {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_otp)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvOtp.text=otp.toString()
        dialog.tvOk.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            val intent=Intent(this, OTPVerificationActivity::class.java)
            intent.putExtra("scr","register")
            intent.putExtra("userId",userId)
            startActivity(intent)
        })
        dialog.show()
    }

    override fun onBackPressed() {
        startActivity(Intent(this@LoginActivity, WelcomeActivity::class.java))
        finishAffinity()
    }

}