package com.techgropse.propertylounge.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterSearch
import com.techgropse.propertylounge.adapter.AdapterSearchProperty
import com.techgropse.propertylounge.model.ResponseHomeProperty
import kotlinx.android.synthetic.main.activity_search.*
import java.util.*
import kotlin.collections.ArrayList

class SearchActivity : AppCompatActivity(),AdapterSearch.RemoveListener {
    var context: Context?=null
    var mainlist= ArrayList<ResponseHomeProperty.Data>()
    var filterList=ArrayList<String>()
    var price=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBackkk.setOnClickListener {
            onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun iniT() {
        context = this
        price=intent.getStringExtra("price")!!
        mainlist = intent.extras!!.getParcelableArrayList<ResponseHomeProperty.Data>("list") as ArrayList<ResponseHomeProperty.Data>
        filterList = (intent.getSerializableExtra("filterlist") as ArrayList<String>?)!!

        if (mainlist.size==0){
            tvPropertyCount.text="0 Property"
            tvNofeatures.visibility=View.VISIBLE
            rvPropertyList.visibility=View.GONE
        }
        else{
            tvPropertyCount.text=mainlist.size.toString()+" Property"
            tvNofeatures.visibility=View.GONE
            rvPropertyList.visibility=View.VISIBLE
            val adapter1 = AdapterSearchProperty(context!!,mainlist)
            rvPropertyList.adapter = adapter1

        }

        filterList.add(price)
        rvsearchlist.adapter=AdapterSearch(filterList,context!!,this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun remove(pos:Int) {
        for (j in pos until filterList.size) {
            filterList.remove(filterList[pos])
            rvsearchlist.adapter=AdapterSearch(filterList,context!!,this)
        }
    }

}