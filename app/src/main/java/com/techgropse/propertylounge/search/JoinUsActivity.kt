package com.techgropse.propertylounge.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.techgropse.propertylounge.R
import kotlinx.android.synthetic.main.activity_join_us.*

class JoinUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_us)

        ivBack.setOnClickListener {
            onBackPressed()
        }
    }
}