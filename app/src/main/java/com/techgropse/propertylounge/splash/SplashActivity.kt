package com.techgropse.propertylounge.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.Constants
import com.techgropse.propertylounge.welcomescreen.WelcomeActivity

class SplashActivity : BaseActivity() {
    private var mPref: SharedPreferenceUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mPref = SharedPreferenceUtils.getInstance(this)
        homestatus()

        Handler().postDelayed({
            if (mPref!!.getBoolean(Constants.IS_LOGGED_IN, false)) {
                startActivity(Intent(this,HomeActivity::class.java))
                finish()
            }
            else{
                startActivity(Intent(this,WelcomeActivity::class.java))
                finish()
            }
        }, 3000)
    }
}