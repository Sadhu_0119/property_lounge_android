package com.techgropse.propertylounge.webservice

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

object ApiClient {
    private val BASE_URL_USER: String = Constants.BASE_URL
    private var retrofit: Retrofit? = null

    val apiClient: Retrofit?
        get() {
            if (retrofit == null) {
                Timber.plant(Timber.DebugTree())
                val loggingInterceptor =
                    HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                        Timber.i(message)
                    })

                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(loggingInterceptor)
                    .connectTimeout(20000, TimeUnit.SECONDS).build()
                retrofit = Retrofit.Builder().baseUrl(
                    BASE_URL_USER
                ).client(client)
                    .addConverterFactory(GsonConverterFactory.create()).build()
            }
            return retrofit
        }

}
