package com.techgropse.propertylounge.webservice

interface Constants {
    companion object {
        const val IS_LOGGED_IN="isUserLogin"
        const val NAME="name"
        const val IMAGE="image"
        const val MOBILE="mobile"
        const val EMAIL="email"
        const val USERID="user_id"
        const val SECURITY_TOKEN="security_token"
        const val DEVICE_TOKEN="device_token"
        const val DEVICE_ID="device_id"

        val LATITUDE="lat"
        val LONGITUDE="longi"

        const val BASE_URL="https://www.goinstablog.com/goinstablog.com/phpDev6Repo/property_lounge/user/"

        //api constants
        internal const val REGISTERUSER = "registerUser"
        internal const val VALIDATEOTP="validateOtp"
        internal const val LOGIN="login"
        internal const val RESENDOTP="resendOtp"
        internal const val FORGOTPASS="forgotPassword"
        internal const val RESETPASS="resetPassword"
        internal const val MYPROFILE="myProfile"
        internal const val UPLOADIMAGEURL="uploadImage"
        internal const val EDITPROFILE="editProfile"
        internal const val AGENTLIST="agentList"
        internal const val NEWSLIST="newsList"
        internal const val NEWSDETAILS="newsDetail"
        internal const val CAREERLIST="careerList"
        internal const val PROPERTYLIST="propertyList"
        internal const val CHANGE_PASS="changePassword"
        internal const val SEARCH_PROPERTY="searchProperty"

    }
}