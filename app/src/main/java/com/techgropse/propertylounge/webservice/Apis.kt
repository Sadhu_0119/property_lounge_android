package com.techgropse.propertylounge.webservice

import com.techgropse.propertylounge.model.*
import com.techgropse.propertylounge.model.location.ModelPlacesNearBy
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface Apis {
    @FormUrlEncoded
    @POST(Constants.REGISTERUSER)
    fun registerUser(
        @Field("password") password: String = "",
        @Field("mobile") mobile: String,
        @Field("email") email: String,
        @Field("full_name") fullName: String,
        @Field("country_code")country_code:String
    ): Call<ResponseRegistration>

    @FormUrlEncoded
    @POST(Constants.VALIDATEOTP)
    fun verifyOTPUser(
        @Field("device_type") device_type: String = "",
        @Field("device_id") device_id: String,
        @Field("user_id") user_id: String,
        @Field("otp") otp: String,
        @Field("device_token") device_token: String = ""
    ): Call<ResponseOTPValidate>

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    fun loginUser(
        @Field("device_type") device_type: String = "",
        @Field("device_id") device_id: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("device_token") device_token: String = ""
    ): Call<ResponseOTPValidate>

    @FormUrlEncoded
    @POST(Constants.RESENDOTP)
    fun resendOTp(
        @Field("user_id") user_id: String
    ): Call<ResponseForgotPass>

    @FormUrlEncoded
    @POST(Constants.FORGOTPASS)
    fun forgotPass(
        @Field("email") email: String
    ): Call<ResponseForgotPass>

    @FormUrlEncoded
    @POST(Constants.RESETPASS)
    fun resetNewPass(
        @Header("Deviceid") device_id: String,
        @Header("Securitytoken")security_token:String,
        @Field("user_id") user_id: String,
        @Field("new_password") new_password: String
    ): Call<ResponseResetPass>

    @FormUrlEncoded
    @POST(Constants.MYPROFILE)
    fun myProfile(
        @Header("Deviceid") device_id: String,
        @Header("Securitytoken")security_token:String,
        @Field("user_id") user_id: String): Call<ResponseMyProfile>

    @Multipart
    @POST(Constants.UPLOADIMAGEURL)
    fun uploadImage(@PartMap part: Map<String, @JvmSuppressWildcards RequestBody>,
                    @Part image: MultipartBody.Part? = null):Call<ResponseImageUpload>

    @FormUrlEncoded
    @POST(Constants.EDITPROFILE)
    fun Editprofile(
        @Header("Deviceid") device_id: String,
        @Header("Securitytoken")security_token:String,
        @Field("user_id") user_id: String,
        @Field("full_name") full_name: String,
        @Field("mobile") mobile: String,
        @Field("country_code")country_code:String,
        @Field("image")image:String
    ): Call<ResponseUpdateProfile>

    @GET(Constants.AGENTLIST)
    fun getAgentList(): Call<ResponseAgentList>

    @GET(Constants.NEWSLIST)
    fun getNewsList(): Call<ResponseNewsList>

    @FormUrlEncoded
    @POST(Constants.NEWSDETAILS)
    fun newsListDeatils(
        @Field("news_id") news_id: String): Call<ResponseNewsDetails>

    @GET(Constants.CAREERLIST)
    fun careerList(): Call<ResponseCareerList>

    @GET(Constants.PROPERTYLIST)
    fun propertyList(): Call<ResponseHomeProperty>

    @FormUrlEncoded
    @POST(Constants.CHANGE_PASS)
    fun changepassword(
        @Header("Deviceid") device_id: String,
        @Header("Securitytoken")security_token:String,
        @Field("user_id") user_id: String,
        @Field("old_password") old_password : String,
        @Field("new_password") new_password: String
    ): Call<ResponseChangePass>

    @FormUrlEncoded
    @POST(Constants.SEARCH_PROPERTY)
    fun searchpropertyList(
        @Field("lat") lat : String,
        @Field("lng") lng : String,
        @Field("available_id")available_id:String,
        @Field("type")type:String,
        @Field("price_min_val")price_min_val:String,
        @Field("price_max_val")price_max_val:String
    ): Call<ResponseHomeProperty>

    @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json")
    fun GetNearByPlaces(
        @Query("location") location: String,
        @Query("radius") radius: String,
        @Query("key") key: String
    ): Call<ModelPlacesNearBy>

}