package com.techgropse.propertylounge.publish

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import kotlinx.android.synthetic.main.activity_publish_with.*

class PublishWithActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publish_with)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {

    }
}