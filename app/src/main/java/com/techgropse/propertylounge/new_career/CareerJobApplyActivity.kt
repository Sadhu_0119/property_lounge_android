package com.techgropse.propertylounge.new_career

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterCareer
import kotlinx.android.synthetic.main.activity_career_apply.*

class CareerJobApplyActivity : AppCompatActivity() {
    var context: Context?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_career_apply)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBackk.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        context = this
    }

}