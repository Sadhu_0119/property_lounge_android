package com.techgropse.propertylounge.new_career

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterNews
import com.techgropse.propertylounge.model.ResponseNewsDetails
import com.techgropse.propertylounge.model.ResponseNewsList
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.act_news.*
import kotlinx.android.synthetic.main.act_news_details_user.*
import kotlinx.android.synthetic.main.act_news_details_user.ivBackk
import kotlinx.android.synthetic.main.card_news.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class NewsDetailsActivity : AppCompatActivity() {
    var mPref: SharedPreferenceUtils?=null
    var userId=""
    var deviceId=""
    var securityToken=""
    var newsRequest: Call<ResponseNewsDetails>?=null
    var context: Context?=null
    var newsId=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_news_details_user)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBackk.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        context = this
        newsId=intent.getStringExtra("newsId")!!
        mPref= SharedPreferenceUtils(this)
        userId=mPref!!.getString(Constants.USERID,"")
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

        ///api  to get news details
        ProgressDialogUtils.getInstance().showProgress(this,false)
        GetNewsDetails()
    }

    ///api  to get news details
    private fun GetNewsDetails() {
        newsRequest = ApiClient.apiClient!!.create(Apis::class.java).newsListDeatils(newsId)
        newsRequest!!.enqueue(object : Callback<ResponseNewsDetails?> {
            override fun onResponse(
                call: Call<ResponseNewsDetails?>,
                response: Response<ResponseNewsDetails?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (!response.body()!!.data.news_detail.image.isNullOrEmpty()) {
                        Picasso.with(context).load(response.body()!!.data.news_detail.image).into(ivnews)
                        tvNewsHead.text=response.body()!!.data.news_detail.title
                        tvnewsDesc.text=response.body()!!.data.news_detail.description
                        val tvDate=response.body()!!.data.news_detail.created_at
                        val separated = tvDate.split(" ")
                        var date=separated[0]
                        var splitdate=date.split("-")
                        var finaldate=splitdate[2]+"/"+splitdate[1]+"/"+splitdate[0]
                        tvdate.text=finaldate
                    }
                }
            }
            override fun onFailure(call: Call<ResponseNewsDetails?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@NewsDetailsActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}