package com.techgropse.propertylounge.new_career

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterNews
import com.techgropse.propertylounge.model.ResponseNewsList
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.act_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsActivity : AppCompatActivity() {
    var mPref: SharedPreferenceUtils?=null
    var userId=""
    var deviceId=""
    var securityToken=""
    var newsRequest: Call<ResponseNewsList>?=null
    var context: Context?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_news)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBackk.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        context = this
        mPref= SharedPreferenceUtils(this)
        userId=mPref!!.getString(Constants.USERID,"")
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

        ///api  to get news list
        ProgressDialogUtils.getInstance().showProgress(this,false)
        GetNewsList()
    }

    ///api  to get news list
    private fun GetNewsList() {
        newsRequest = ApiClient.apiClient!!.create(Apis::class.java).getNewsList()
        newsRequest!!.enqueue(object : Callback<ResponseNewsList?> {
            override fun onResponse(
                call: Call<ResponseNewsList?>,
                response: Response<ResponseNewsList?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.data.news_list.isNullOrEmpty()){
                        newslist.visibility=View.VISIBLE
                        rvNewList.visibility=View.GONE
                    }
                    else{
                        newslist.visibility=View.GONE
                        rvNewList.visibility=View.VISIBLE
                        val list=response.body()!!.data.news_list
                        val adapter1 = AdapterNews(context!!,list)
                        rvNewList.adapter = adapter1
                    }
                }
            }
            override fun onFailure(call: Call<ResponseNewsList?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@NewsActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}