package com.techgropse.propertylounge.new_career

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterCareer
import com.techgropse.propertylounge.model.ResponseCareerList
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.act_career.*
import kotlinx.android.synthetic.main.act_career.ivBackk
import kotlinx.android.synthetic.main.act_news.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class CareerActivity : AppCompatActivity() {
    var mPref: SharedPreferenceUtils?=null
    var userId=""
    var deviceId=""
    var securityToken=""
    var careerRequest: Call<ResponseCareerList>?=null
    var context: Context?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_career)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBackk.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        context = this
        mPref= SharedPreferenceUtils(this)
        userId=mPref!!.getString(Constants.USERID,"")
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

        ///api  to get career list
        ProgressDialogUtils.getInstance().showProgress(this,false)
        GetCareerList()

    }

    ///api  to get career list
    private fun GetCareerList() {
        careerRequest = ApiClient.apiClient!!.create(Apis::class.java).careerList()
        careerRequest!!.enqueue(object : Callback<ResponseCareerList?> {
            override fun onResponse(
                call: Call<ResponseCareerList?>,
                response: Response<ResponseCareerList?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.data.career_list.isNullOrEmpty()){
                        careerlist.visibility= View.VISIBLE
                        rvCareerList.visibility= View.GONE
                    }
                    else{
                        careerlist.visibility= View.GONE
                        rvCareerList.visibility= View.VISIBLE
                        val list=response.body()!!.data.career_list
                        val adapter1 = AdapterCareer(context!!,list)
                        rvCareerList.adapter = adapter1
                    }
                }
            }
            override fun onFailure(call: Call<ResponseCareerList?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@CareerActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

}