package com.techgropse.propertylounge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseHomeProperty(
    val data: ArrayList<Data>,
    val error: Boolean,
    val error_code: Int,
    val message: String
):Parcelable {

    @Parcelize
    data class Data(
        val agent_id: String,
        val no_of_property:String?=null,
        val agent_name: String?=null,
        val available_id: String?=null,
        val carpet_area: String,
        val city_id: String,
        val cover_image: String,
        val created_at: String,
        val description: String,
        val description_ar: String,
        val description_fr: String,
        val features: ArrayList<Feature>,
        val financial_status: String,
        val house_no: String,
        val images: String,
        val is_featured: String,
        val latitude: String,
        val location: String,
        val longitude: String,
        val name: String,
        val name_ar: String,
        val owner_name: String,
        val price: String,
        val property_id: String,
        val property_owner: String,
        val property_type_id: String,
        val rating: String,
        val status: String,
        val super_area: String,
        val type: String,
        val video: String,
        val viewer: String
    ) :Parcelable{
        @Parcelize
        data class Feature(
            val facilities_value_id: String?=null,
            val name: String
        ):Parcelable
    }
}