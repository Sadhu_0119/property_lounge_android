package com.techgropse.propertylounge.model

data class ResponseMyProfile(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val country_code: String,
        val email: String,
        val full_name: String,
        val image: String,
        val mobile: String,
        val user_id: String
    )
}