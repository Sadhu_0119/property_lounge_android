package com.techgropse.propertylounge.model

data class ResponseSearchSelf(
    val name: String
)