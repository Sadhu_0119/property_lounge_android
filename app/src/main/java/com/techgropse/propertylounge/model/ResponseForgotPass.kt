package com.techgropse.propertylounge.model

data class ResponseForgotPass(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val user_login_info: UserLoginInfo
    ) {
        data class UserLoginInfo(
            val otp: Int,
            val user_id: String
        )
    }
}