package com.techgropse.propertylounge.model

data class ResponseRegistration(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val otp: Int,
        val user_id: String)
}