package com.techgropse.propertylounge.model

data class ResponseNewsList(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val news_list: List<News>
    ) {
        data class News(
            val created_at: String,
            val description: String,
            val image: String?=null,
            val news_id: String,
            val posted_by: String,
            val title: String,
            val user_id: String
        )
    }
}