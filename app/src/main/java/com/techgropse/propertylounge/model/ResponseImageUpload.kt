package com.techgropse.propertylounge.model

data class ResponseImageUpload(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val image: String
    )
}