package com.techgropse.propertylounge.model

data class ResponseCareerList(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val career_list: List<Career>
    ) {
        data class Career(
            val created_at: String,
            val description: String,
            val job_id: String,
            val no_of_position: String,
            val title: String,
            val updated_at: String
        )
    }
}