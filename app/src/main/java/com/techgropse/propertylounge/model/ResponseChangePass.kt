package com.techgropse.propertylounge.model

data class ResponseChangePass(
    val error: Boolean,
    val error_code: Int,
    val message: String,
    val data: Data
) {
    class Data(
    )
}