package com.techgropse.propertylounge.model

data class ResponseAgentList(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val agent_list: List<Agent>
    ) {
        data class Agent(
            val agency_id: String,
            val agency_name: String,
            val agent_id: String,
            val country_code: String,
            val designation: String,
            val email: String,
            val full_name: String,
            val image: String,
            val lat: String,
            val lng: String,
            val location: String,
            val mobile: String,
            val no_of_property: String
        )
    }
}