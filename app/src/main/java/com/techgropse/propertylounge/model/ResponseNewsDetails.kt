package com.techgropse.propertylounge.model

data class ResponseNewsDetails(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val news_detail: NewsDetail,
        val recent_news: List<RecentNew>
    ) {
        data class NewsDetail(
            val created_at: String,
            val description: String,
            val image: String?=null,
            val news_id: String,
            val posted_by: String,
            val title: String,
            val user_id: String
        )

        data class RecentNew(
            val created_at: String,
            val description: String,
            val image: String,
            val news_id: String,
            val posted_by: String,
            val title: String,
            val user_id: String
        )
    }
}