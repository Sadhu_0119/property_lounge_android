package com.techgropse.propertylounge.model

data class ResponseOTPValidate(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    data class Data(
        val user_login_info: UserLoginInfo
    ) {
        data class UserLoginInfo(
            val country_code: String,
            val device_id: String,
            val email: String,
            val full_name: String,
            val image: String,
            val mobile: String,
            val security_token: String,
            val user_id: String,
            val otp:Int
        )
    }
}