package com.techgropse.propertylounge.model

data class ResponseResetPass(
    val `data`: Data,
    val error: Boolean,
    val error_code: Int,
    val message: String
) {
    class Data(
    )
}