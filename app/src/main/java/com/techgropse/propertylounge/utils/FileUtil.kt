package com.techgropse.propertylounge.utils

import android.app.DownloadManager
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.webkit.MimeTypeMap
import java.io.*
import java.net.URLConnection
import java.net.URLEncoder
import java.text.Normalizer
import java.text.SimpleDateFormat
import java.util.*

/*https://stackoverflow.com/questions/35528409/write-a-large-inputstream-to-file-in-kotlin*/
fun File.copyInputStreamToFile(inputStream: InputStream) {
    inputStream.use { input ->
        this.outputStream().use { fileOut ->
            input.copyTo(fileOut)
        }
    }
}

fun File.saveBitmap(bitmap: Bitmap) {
    val bos = ByteArrayOutputStream()
    bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
    val bitmapData = bos.toByteArray()
    val byteArrayInputStream = ByteArrayInputStream(bitmapData)
    byteArrayInputStream.use { input ->
        this.outputStream().use { fileOut ->
            input.copyTo(fileOut)
        }
    }
}

/*https://stackoverflow.com/a/11537115/2437655*/
val String.alphaNumericFileName: String
    get() = Normalizer.normalize(this, Normalizer.Form.NFD).replace("[^A-Za-z0-9.]".toRegex(), "")


object FileUtil {

    val TAG = FileUtil::class.simpleName!!

    val DIR_SUFFIX_TEXT = "/Text/"
    val DIR_SUFFIX_IMAGES = "/Images/"
    val DIR_SUFFIX_AUDIOS = "/Audios/"
    val DIR_SUFFIX_DOCUMENTS = "/Documents/"
    val DIR_SUFFIX_VIDEOS = "/Videos/"
    val DIR_SUFFIX_FONTS = "/Fonts/"
    val DIR_SUFFIX_FILES = "/Files/"

    val THUMB_COLUMNS = arrayOf(MediaStore.Video.Thumbnails.DATA)
    val MEDIA_COLUMNS = arrayOf(MediaStore.Video.Media._ID)

    val DEFAULT_TEXT_EXT = ".txt"
    val DEFAULT_IMAGE_EXT = ".jpg"
    val DEFAULT_VIDEO_EXT = ".mp4"

    val DEFAULT_TEXT_PREFIX = "TEXT_"
    val DEFAULT_IMAGE_PREFIX = "IMG_"
    val DEFAULT_VIDEO_PREFIX = "VID_"

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * Ref: @link [https://gist.github.com/tatocaster/32aad15f6e0c50311626]
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    fun getRealPath(context: Context, uri: Uri): String? {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            when {
                isExternalStorageDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    // this does not handle non-primary volumes
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                }
                isDownloadsDocument(uri) -> {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)!!)
                    return getDataColumn(context, contentUri, null, null)
                }
                isMediaDocument(uri) -> {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    var contentUri: Uri? = null
                    when (type) {
                        "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }

                    val selection = "_id=?"
                    val selectionArgs = arrayOf(split[1])

                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            }
        }

        // Return the remote address
        else if ("content".equals(uri.scheme, ignoreCase = true))
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)

        // File
        else if ("file".equals(uri.scheme, ignoreCase = true)) return uri.path

        // Default
        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    fun getThumbnailPath(context: Context, fileUri: Uri): String? {
        val fileId = getFileId(context, fileUri)
        if (fileId == 0L) return null

        MediaStore.Video.Thumbnails.getThumbnail(context.contentResolver, fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null)

        context.contentResolver.query(
                MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                THUMB_COLUMNS,
                MediaStore.Video.Thumbnails.VIDEO_ID + " = " + fileId,
                null,
                null
        )?.use {
            if (it.moveToFirst()) {
                return it.getString(it.getColumnIndex(MediaStore.Video.Thumbnails.DATA))
            }
        }
        return null
    }

    private fun getFileId(context: Context, fileUri: Uri): Long {
        context.contentResolver.query(fileUri, MEDIA_COLUMNS, null, null, null)?.use {
            try {
                if (it.moveToFirst()) {
                    val columnIndex = it.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                    return it.getInt(columnIndex).toLong()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return 0L
    }


    fun getExtention(context: Context, uri: Uri): String? {
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(context.contentResolver.getType(uri))
    }

    fun getMimeType(context: Context, uri: Uri): String? {
        return context.contentResolver.getType(uri)
    }

    fun isImageFile(path: String): Boolean {
        val mimeType = URLConnection.guessContentTypeFromName(path)
        return mimeType?.startsWith("image") ?: false
    }

    fun isVideoFile(path: String): Boolean {
        val mimeType = URLConnection.guessContentTypeFromName(path)
        return mimeType?.startsWith("video") ?: false
    }


    fun downloadFile(context: Context, url: String, title: String, description: String, subpath: String) {
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri)
        request.setTitle(title)
        request.setDescription(description)
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, subpath)
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        downloadManager.enqueue(request)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    fun getThumbnailPathS(context: Context, fileUri: Uri): String? {
        val fileId = getFileIdS(context, fileUri)
        if (fileId == 0L) return null

        MediaStore.Video.Thumbnails.getThumbnail(context.contentResolver, fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null)

        context.contentResolver.query(
                MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                THUMB_COLUMNS,
                MediaStore.Video.Thumbnails.VIDEO_ID + " = " + fileId,
                null,
                null
        )?.use {
            if (it.moveToFirst()) {
                return it.getString(it.getColumnIndex(MediaStore.Video.Thumbnails.DATA))
            }
        }
        return null
    }

    fun getFileIdS(context: Context, fileUri: Uri): Long {
        context.contentResolver.query(fileUri, MEDIA_COLUMNS, null, null, null)?.use {
            try {
                if (it.moveToFirst()) {
                    val columnIndex = it.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
                    return it.getInt(columnIndex).toLong()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return 0L
    }


}
