package com.techgropse.propertylounge.utils
import android.app.Activity
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.ImageView
import androidx.exifinterface.media.ExifInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import jp.wasabeef.glide.transformations.BlurTransformation
import java.io.*

@Suppress("unused")
object ImageUtils {
    val TAG = ImageUtils::class.simpleName!!

    private const val MAX_HEIGHT = 720.0f
    private const val MAX_WIDTH = 720.0f

    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String): Bitmap {
        val ei = ExifInterface(image_absolute_path)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        return when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate(bitmap, 270f)
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flip(bitmap, true, false)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> flip(bitmap, false, true)
            else -> bitmap
        }
    }

    private fun rotate(bitmap: Bitmap, degrees: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
        val matrix = Matrix()
        matrix.preScale((if (horizontal) -1 else 1).toFloat(), (if (vertical) -1 else 1).toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun setImage(
        imageView: ImageView,
        isCenterCrop: Boolean = false,
        isCenterInside: Boolean = false,
        isRoundCorners: Boolean = false,
        isCircleCrop: Boolean = false,
        isBlurImage: Boolean = false,
        roundingRadius: Int = 5.toDp,
        load: Any?,
        placeholderResId: Any? = null,
        errorResId: Any? = null
    ) {
        if (load is String && load.isEmpty()) return
        if (!isValidContextForGlide(imageView.context)) return

        Glide.with(imageView.context).load(load).apply {
            if (isCenterCrop) centerCrop()
            if (isCenterInside) centerInside()
            if (isCircleCrop) circleCrop()
            if (isRoundCorners) transform(RoundedCorners(roundingRadius))
            if (isBlurImage) transform(BlurTransformation())
            placeholderResId?.let {
                when (it) {
                    is Drawable -> placeholder(it)
                    is Int -> placeholder(it)
                    else -> {
                    }
                }
            }

            errorResId?.let {
                when (it) {
                    is Drawable -> error(it)
                    is Int -> error(it)
                    else -> {
                    }
                }

            }
            into(imageView)
        }
    }

    /*https://github.com/bumptech/glide/issues/1484*/
    // You cannot start a load for a destroyed activity
    private fun isValidContextForGlide(context: Context?): Boolean {
        if (context == null) return false

        if (context is Activity) {
            if (context.isDestroyed || context.isFinishing) {
                return false
            }
        }
        return true
    }

    fun fixOrientation(bitmap: Bitmap): Bitmap {
        if (bitmap.width > bitmap.height) {
            val matrix = Matrix()
            matrix.postRotate(90f)
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        }
        return bitmap
    }

    fun getImageUri(context: Context, inImage: Bitmap): Uri {
        @Suppress("UNUSED_VARIABLE")
        val bytes = ByteArrayOutputStream()
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, inImage, "Title", null)
        Log.d(TAG, "getImageUri() returned: " + path)
        return Uri.parse(path)
    }

    fun resize(originalBitmap: Bitmap, size: Int): Bitmap {
        val w = originalBitmap.width
        val h = originalBitmap.height
        if (Math.max(w, h) > size) {
            val scalledW: Int
            val scalledH: Int
            if (w <= h) {
                scalledW = (w / (h.toDouble() / size)).toInt()
                scalledH = size
            } else {
                scalledW = size
                scalledH = (h / (w.toDouble() / size)).toInt()
            }
            val result = Bitmap.createScaledBitmap(originalBitmap, scalledW, scalledH, true)
            if (!originalBitmap.isRecycled) {
                originalBitmap.recycle()
            }
            return result
        } else {
            return originalBitmap
        }
    }

    fun compressImage(context: Context, imagePath: String): String? {
        Log.d(TAG, "compressImage()")
        val uncompressedFile = File(imagePath)
        Log.d(TAG, "Uncompressed File Size: ${uncompressedFile.length()}")
        var scaledBitmap: Bitmap?

        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true

        @Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
        var bmp = BitmapFactory.decodeFile(imagePath, options)
        var actualHeight = options.outHeight
        var actualWidth = options.outWidth

        var imgRatio = actualWidth.toFloat() / actualHeight.toFloat()
        val maxRatio = MAX_WIDTH / MAX_HEIGHT

        if (actualHeight > MAX_HEIGHT || actualWidth > MAX_WIDTH) {
            if (imgRatio < maxRatio) {
                imgRatio = MAX_HEIGHT / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = MAX_HEIGHT.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = MAX_WIDTH / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = MAX_WIDTH.toInt()
            } else {
                actualHeight = MAX_HEIGHT.toInt()
                actualWidth = MAX_WIDTH.toInt()

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)
        options.inJustDecodeBounds = false
        options.inDither = false
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
            return null
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
            return null
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap)
        canvas.getMatrix(scaleMatrix)
        canvas.drawBitmap(bmp, middleX - bmp!!.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))

        bmp.recycle()

        val exif: ExifInterface
        try {
            exif = ExifInterface(imagePath)
            val orientation = exif.getAttributeInt(android.media.ExifInterface.TAG_ORIENTATION, android.media.ExifInterface.ORIENTATION_UNDEFINED)
            val matrix = Matrix()
            when (orientation) {
                android.media.ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
                android.media.ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
                android.media.ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap!!, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val out: FileOutputStream?
        val filepath = getFilename(context)
        try {
            out = FileOutputStream(filepath)
            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            return null
        }

        val compressedFile = File(filepath)
        Log.d(TAG, "Compressed File Size: ${compressedFile.length()}")

        return filepath
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }

        return inSampleSize
    }

    private fun getFilename(context: Context): String {
        val mediaStorageDir = File("${Environment.getExternalStorageDirectory()}/Android/data/${context.applicationContext.packageName}/Files/Compressed")
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs()
        }

        val mImageName = "IMG_" + System.currentTimeMillis().toString() + ".jpg"
        return mediaStorageDir.absolutePath + "/" + mImageName
    }

}