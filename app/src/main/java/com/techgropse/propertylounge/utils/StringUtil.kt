package com.techgropse.propertylounge.utils

import android.app.Activity
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


fun String.isValidPassword() = length >= 8

fun String.isValidEmail() = Pattern.compile(
        "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
).matcher(this).matches()

fun String.isValidFB() = Pattern.compile(
        ".+www.facebook.com\\/[^\\/]+\$"
).matcher(this).matches()

fun isValidUrl(url: String): Boolean {
    val p = Patterns.WEB_URL
    val m = p.matcher(url.toLowerCase())
    return m.matches()
}

fun String.formatToPhone() {
    Date().time
}



val String.isValidMobile: Boolean
    get() = this.length in 9..15

/*fun String.isValidOtp() = length == 5*/

fun String.getDate(format: String): String =
        try {
            SimpleDateFormat(format, Locale.US).format(Date(this.toLong().times(1000)))
        } catch (e: Exception) {
            this
        }

fun String.getDateMillis(format: String): String =
        try {
            SimpleDateFormat(format, Locale.US).format(Date(this.toLong()))
        } catch (e: Exception) {
            this
        }

fun calcDifference(startDate: String, endDate: String, format: String): String {
    try {
        //Dates to compare

        val date1: Date
        val date2: Date

        val dates = SimpleDateFormat(format)

        //Setting dates
        date1 = dates.parse(endDate)
        date2 = dates.parse(startDate)

        //Comparing dates
        val difference = Math.abs(date1.time - date2.time)
        val differenceDates = difference / (24 * 60 * 60 * 1000)

        //Convert long to String
        val dayDifference = java.lang.Long.toString(differenceDates)

        Log.e("HERE", "HERE: $dayDifference")
        return dayDifference

    } catch (exception: Exception) {
        Log.e("DIDN'T WORK", "exception $exception")
        return "-"
    }
}

fun String.convertFormat(formatInput: String, formatOutPut: String): String =
        try {
            SimpleDateFormat(formatOutPut, Locale.US).format(SimpleDateFormat(formatInput, Locale.US).parse(this))
        } catch (e: Exception) {
            this
        }

/**
 * Get Date Object from time string representation
 * Millis in date is always in UTC, You current time zone milles is stored in offset not in time inside date and calendar.
 */
fun String.getTimeStampMillis(format: String): Date =
        try {
            SimpleDateFormat(format, Locale.US).parse(this)
        } catch (e: Exception) {
            Date()
        }

fun Activity.hideKeyboard() {
    val view: View? = if (currentFocus == null) View(this) else currentFocus
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
}

fun String.isValidOtp() = length == 4
