package com.techgropse.propertylounge.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*



public class DateUtils {

    companion object {

        fun   concatenatingDateAndTime(date: String, time: String):String
        {
            val mAlertDate = "10 Mar 2016"
            val mAlertTime = "8:00 PM"
            val timeStampFormat = SimpleDateFormat("d MMM yyyy hh:mm aa")
            val mAlertDateTime = date + " " + time



            var  date = timeStampFormat.parse(mAlertDateTime)
            val simpleDateFormat = SimpleDateFormat("E MMM dd HH:mm:ss zzzz yyyy")
            var  date2 = simpleDateFormat.parse(simpleDateFormat.format(date))


            val output = date.getTime() / 1000L
            val str = java.lang.Long.toString(output)
            var timestamp = java.lang.Long.parseLong(str)
            Log.e("timestamp",timestamp.toString())
            return timestamp.toString()
        }

        fun getTimeFromTimestamp( timestamp : String) : String{
            var time : String=""
            try {
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = timestamp.toLong()
                val sdf = SimpleDateFormat("h:mm a")
                val currenTimeZone = calendar.time as Date
                return sdf.format(currenTimeZone)
            } catch (e: Exception) {
            }

            return time
        }

        fun getTimeFromTimestamp1( timestamp : String) : String{
            var time : String=""
            try {
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = timestamp.toLong()
                val sdf = SimpleDateFormat("h")
                val currenTimeZone = calendar.time as Date
                return sdf.format(currenTimeZone)
            } catch (e: Exception) {
            }

            return time
        }

        fun getDateFromTimestamp(timestamp: String) : String{
            var time : String=""
            try {
                val calendar = Calendar.getInstance()
                calendar.timeInMillis = timestamp.toLong()
                val sdf = SimpleDateFormat("dd MMM yyyy")
                val currenTimeZone = calendar.time as Date
                return sdf.format(currenTimeZone)
            } catch (e: Exception) {
            }

            return time
        }
    }
}
//   13 Mar 2019 0:0 AM  driver side   1552415400000
//   13 Mar 2019 0:0 AM  driver side   1552415400000