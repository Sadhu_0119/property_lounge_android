package com.techgropse.propertylounge.utils

import android.content.res.Resources
import android.os.Build
import android.text.Html

val Int.toDp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

fun String.encodeHtmlToString():String{
    val string = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }

    return string.toString()
}