package com.techgropse.propertylounge.resetpass

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.forgotpass.ForgotPasswordActivity
import com.techgropse.propertylounge.login_sinup.LoginActivity
import com.techgropse.propertylounge.model.ResponseResetPass
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.utils.isValidPassword
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.dialog_passchange.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResetPasswordActivity : BaseActivity() {
    var userid=""
    var resetRequest: Call<ResponseResetPass>?=null
    var deviceId=""
    var securityToken=""
    var mPref: SharedPreferenceUtils?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        iniT()
        initControl()
    }

    private fun initControl() {
        backblack.setOnClickListener {
            onBackPressed()
        }

        btnSubmit.setOnClickListener {
            val password=edpassword.text.toString().trim()
            val repassword=edconfirmpassword.text.toString().trim()

            if (password.isEmpty()) {
                Toast.makeText(this,resources.getString(R.string.error_password), Toast.LENGTH_SHORT).show()
            } else if (!password.isValidPassword()) {
                Toast.makeText(this,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }else  if (repassword.isEmpty()) {
                Toast.makeText(this,resources.getString(R.string.error_repassword), Toast.LENGTH_SHORT).show()
            } else if (!repassword.isValidPassword()) {
                Toast.makeText(this,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }
            else if(password != repassword){
                Toast.makeText(this,resources.getString(R.string.error_same_password), Toast.LENGTH_SHORT).show()
            }
            else{
                //api call to reset password
                ProgressDialogUtils.getInstance().showProgress(this,false)
                ApiResetPassword(userid,password)
            }
        }
}

    //api call to reset password
    private fun ApiResetPassword(userid: String, password: String) {
        resetRequest = ApiClient.apiClient!!.create(Apis::class.java).resetNewPass(deviceId,securityToken,userid,password)
        resetRequest!!.enqueue(object : Callback<ResponseResetPass?> {
            override fun onResponse(
                call: Call<ResponseResetPass?>,
                response: Response<ResponseResetPass?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        dialogopen()
                    }
                    else{
                        Toast.makeText(this@ResetPasswordActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            override fun onFailure(call: Call<ResponseResetPass?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@ResetPasswordActivity,t.message,Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen() {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_passchange)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvoOk.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
            finish()
        }
        dialog.show()
    }

    private fun iniT() {
        userid=intent.getStringExtra("userId")!!
        mPref= SharedPreferenceUtils(this)
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

    }

    override fun onBackPressed() {
       startActivity(Intent(this,ForgotPasswordActivity::class.java))
        finish()
    }
}