package com.techgropse.propertylounge.forgotpass

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.model.ResponseForgotPass
import com.techgropse.propertylounge.otp.OTPVerificationActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.utils.isValidEmail
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.dialog_otp.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : BaseActivity() {
    var forgotuserRequest: Call<ResponseForgotPass>? = null
    var mPref: SharedPreferenceUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        iniT()
        initcontrol()
    }

    private fun iniT() {
        mPref = SharedPreferenceUtils(this)
    }

    private fun initcontrol() {
        btnSubmit.setOnClickListener {
            val email = edemail.text.toString().trim()
            if (email.isEmpty()) {
                Toast.makeText(this,resources.getString(R.string.error_email), Toast.LENGTH_SHORT).show()
            } else if (!email.isValidEmail()) {
                Toast.makeText(this,resources.getString(R.string.error_validemail), Toast.LENGTH_SHORT).show()
            } else {
                //api for forgot password
                ProgressDialogUtils.getInstance().showProgress(this, false)
                ApiForgotPassUser(email)
            }
        }

        backblack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun ApiForgotPassUser(email: String) {
        forgotuserRequest = ApiClient.apiClient!!.create(Apis::class.java).forgotPass(email)
        forgotuserRequest!!.enqueue(object : Callback<ResponseForgotPass?> {
            override fun onResponse(
                call: Call<ResponseForgotPass?>,
                response: Response<ResponseForgotPass?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        dialogopen(response.body()!!.data.user_login_info.otp.toString(),response.body()!!.data.user_login_info.user_id)
                    }
                    else{
                        Toast.makeText(this@ForgotPasswordActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }
                }

            }
            override fun onFailure(call: Call<ResponseForgotPass?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@ForgotPasswordActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen(otp: String, userid: String) {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_otp)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvOtp.text=otp
        dialog.tvOk.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            val intent = Intent(this, OTPVerificationActivity::class.java)
            intent.putExtra("scr", "forgot")
            intent.putExtra("userId",userid)
            startActivity(intent)
        })
        dialog.show()
    }

}