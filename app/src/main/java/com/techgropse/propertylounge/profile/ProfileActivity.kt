package com.techgropse.propertylounge.profile

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.model.ResponseMyProfile
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import com.techgropse.propertylounge.welcomescreen.WelcomeActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.ivBack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileActivity : BaseActivity() {
    var mPref: SharedPreferenceUtils? = null
    var userId = ""
    var deviceId = ""
    var securityToken = ""
    var profilerequest: Call<ResponseMyProfile>? = null
    var code=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        iniT()
        iniTcontrol()
    }

    private fun iniT() {
        mPref = SharedPreferenceUtils(this)
        userId = mPref!!.getString(Constants.USERID, "")
        deviceId = mPref!!.getString(Constants.DEVICE_ID, "")
        securityToken = mPref!!.getString(Constants.SECURITY_TOKEN, "")

        //api to get profile data
        ProgressDialogUtils.getInstance().showProgress(this, false)
        GetProfiledata(userId, securityToken, deviceId)

    }

    //api to get profile data
    private fun GetProfiledata(userId: String?, securityToken: String?, deviceId: String?) {
        profilerequest = ApiClient.apiClient!!.create(Apis::class.java).myProfile(deviceId!!, securityToken!!, userId!!)
        profilerequest!!.enqueue(object : Callback<ResponseMyProfile?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<ResponseMyProfile?>,
                response: Response<ResponseMyProfile?>
            ) {
                if (response.isSuccessful) {
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        name.text = response.body()!!.data.full_name

                        if (response.body()!!.data.country_code=="0"){
                            code="+971"
                        }
                        else{
                           code="+"+response.body()!!.data.country_code
                        }
                        email.text = response.body()!!.data.email
                        if (response.body()!!.data.mobile.isNullOrEmpty()){
                            mobile.text = "Mobile no not added"
                        }
                        else {
                            mobile.text = code + " " + response.body()!!.data.mobile
                        }
                        if (!response.body()!!.data.image.isNullOrEmpty()) {
                            Picasso.with(this@ProfileActivity).load(response.body()!!.data.image).into(ivPerson)
                        }
                        else{
                            Picasso.with(this@ProfileActivity).load(R.drawable.dummyplaceholder).into(ivPerson)
                        }
                    }
                    else if (response.body()!!.error_code==301){
                        Toast.makeText(this@ProfileActivity, "Authrntication failed,Please Login Again.", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this@ProfileActivity,WelcomeActivity::class.java))
                        finish()
                    }
                    else{
                        Toast.makeText(this@ProfileActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()

                    }

                }
            }

            override fun onFailure(call: Call<ResponseMyProfile?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@ProfileActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun iniTcontrol() {

        llprofile.setOnClickListener {
            startActivity(Intent(this,EditProfileActivity::class.java))
            finish()
        }

        ivBack.setOnClickListener {
            onBackPressed()
        }

        tvcontinuesearch.setOnClickListener {
            startActivity(Intent(this,HomeActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this,HomeActivity::class.java))
        finish()
    }
}