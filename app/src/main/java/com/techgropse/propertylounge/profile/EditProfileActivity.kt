package com.techgropse.propertylounge.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.hbb20.CountryCodePicker
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.changepass.ChangePasswordActivity
import com.techgropse.propertylounge.model.ResponseImageUpload
import com.techgropse.propertylounge.model.ResponseMyProfile
import com.techgropse.propertylounge.model.ResponseUpdateProfile
import com.techgropse.propertylounge.utils.*
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import com.techgropse.propertylounge.welcomescreen.WelcomeActivity
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*

class EditProfileActivity : BaseActivity() {
    var mPref: SharedPreferenceUtils? = null
    var userId = ""
    var deviceId = ""
    var securityToken = ""
    var profilerequest: Call<ResponseMyProfile>? = null
    var imageupload: Call<ResponseImageUpload>? = null
    private val RC_PICK_PROFILE_IMAGE = 1001
    private val RC_PERM_PICK_IMAGE = 2001
    lateinit var activity: Activity
    var currentProfileFile: File? = null
    var imageurl = ""
    var profileupdaterequest: Call<ResponseUpdateProfile>? = null
    var countryCodeAndroid=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        iniT()
        iniTcontrol()
    }

    private fun iniT() {
        mPref = SharedPreferenceUtils(this)
        userId = mPref!!.getString(Constants.USERID, "")
        deviceId = mPref!!.getString(Constants.DEVICE_ID, "")
        securityToken = mPref!!.getString(Constants.SECURITY_TOKEN, "")
        activity = this

        ccp.setOnCountryChangeListener(object : CountryCodePicker.OnCountryChangeListener {
            override fun onCountrySelected() {
                tvcountryCode.visibility=View.VISIBLE
                tvcountryCode.text = ccp.selectedCountryCodeWithPlus
                countryCodeAndroid = ccp.selectedCountryCodeWithPlus
            }
        })

        //api to get profile data
        ProgressDialogUtils.getInstance().showProgress(this, false)
        GetProfiledata(userId, securityToken, deviceId)
    }

    private fun GetProfiledata(userId: String?, securityToken: String?, deviceId: String?) {
        profilerequest = ApiClient.apiClient!!.create(Apis::class.java)
            .myProfile(deviceId!!, securityToken!!, userId!!)
        profilerequest!!.enqueue(object : Callback<ResponseMyProfile?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<ResponseMyProfile?>,
                response: Response<ResponseMyProfile?>
            ) {
                if (response.isSuccessful) {
                    ProgressDialogUtils.getInstance().hideProgress()

                    if (response.body()!!.error_code == 200) {
                        edName.setText(response.body()!!.data.full_name)
                        if (response.body()!!.data.country_code == "0") {
                            tvcountryCode.text = "+971"
                            countryCodeAndroid="+971"
                        } else {
                            tvcountryCode.text = "+"+response.body()!!.data.country_code
                            countryCodeAndroid=response.body()!!.data.country_code
                        }
                        edMobile.setText(response.body()!!.data.mobile)
                        edEmail.setText(response.body()!!.data.email)

                        if (!response.body()!!.data.image.isNullOrEmpty()) {
                            imageurl = response.body()!!.data.image
                            Picasso.with(this@EditProfileActivity)
                                .load(response.body()!!.data.image).into(ivEditPic)
                        } else {
                            Picasso.with(this@EditProfileActivity).load(R.drawable.dummyplaceholder)
                                .into(ivEditPic)
                        }
                    } else if (response.body()!!.error_code == 301) {
                        Toast.makeText(
                            this@EditProfileActivity,
                            "Authentication failed,Please Login Again.",
                            Toast.LENGTH_SHORT
                        ).show()
                        startActivity(Intent(this@EditProfileActivity, WelcomeActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(
                            this@EditProfileActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()

                    }
                }
            }

            override fun onFailure(call: Call<ResponseMyProfile?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@EditProfileActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun iniTcontrol() {
        tvChangepass.setOnClickListener {
            startActivity(Intent(this, ChangePasswordActivity::class.java))
        }

        ivBack.setOnClickListener {
            onBackPressed()
        }

        rlProfile.setOnClickListener {
            pickFile()
        }

        btnSubmit.setOnClickListener {
            val name = edName.text.toString().trim()
            val mobile = edMobile.text.toString().trim()

            if (name.isEmpty()) {
                Toast.makeText(this@EditProfileActivity, "Please enter name", Toast.LENGTH_SHORT)
                    .show()
            } else if (mobile.isEmpty()) {
                Toast.makeText(
                    this@EditProfileActivity,
                    resources.getString(R.string.error_mobile),
                    Toast.LENGTH_SHORT
                ).show()
            } else if (!mobile.isValidMobile) {
                Toast.makeText(
                    this@EditProfileActivity,
                    resources.getString(R.string.error_validmobile),
                    Toast.LENGTH_SHORT
                ).show()
            } else if (imageurl == "") {
                Toast.makeText(this@EditProfileActivity, "Please upload image", Toast.LENGTH_SHORT)
                    .show()
            } else {
                ///api to update profile
                ProgressDialogUtils.getInstance().showProgress(this, false)
                UpdateProfile(name, mobile, imageurl)
            }
        }

    }

    ///api to update profile
    private fun UpdateProfile(name: String, mobile: String, imageurl: String) {
        profileupdaterequest = ApiClient.apiClient!!.create(Apis::class.java).Editprofile(
            deviceId,
            securityToken,
            userId,
            name,
            mobile,
            countryCodeAndroid,
            imageurl
        )
        profileupdaterequest!!.enqueue(object : Callback<ResponseUpdateProfile?> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(
                call: Call<ResponseUpdateProfile?>,
                response: Response<ResponseUpdateProfile?>
            ) {
                if (response.isSuccessful) {
                    ProgressDialogUtils.getInstance().hideProgress()

                    if (response.body()!!.error_code == 200) {
                        Toast.makeText(
                            this@EditProfileActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        mPref!!.putString(Constants.IMAGE, (response.body()!!.data.image))
                        mPref!!.putString(Constants.NAME, (response.body()!!.data.full_name))
                        startActivity(Intent(this@EditProfileActivity, ProfileActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(
                            this@EditProfileActivity,
                            response.body()!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseUpdateProfile?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@EditProfileActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun pickFile() {
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                RC_PERM_PICK_IMAGE
            )
        } else {
            selectImage()
        }
    }

    private fun selectImage() {
        val intent = CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setFixAspectRatio(true)
            .getIntent(activity)
        activity.startActivityForResult(intent, RC_PICK_PROFILE_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_PICK_PROFILE_IMAGE -> {
                when (resultCode) {
                    AppCompatActivity.RESULT_OK -> {
                        CropImage.getActivityResult(data).uri?.let { uri ->
                            Single.fromCallable {
                                ImageUtils.compressImage(
                                    this, FileUtil.getRealPath(this, uri)
                                        ?: throw NullPointerException("Cannot process image.. URI: $uri")
                                )
                            }
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    {
                                        currentProfileFile = File(it)
                                        ImageUtils.setImage(
                                            imageView = ivEditPic,
                                            load = currentProfileFile
                                        )
                                        ProgressDialogUtils.getInstance().showProgress(this, false)
                                        UploadImage(currentProfileFile!!, userId, "1")
                                    },

                                    {
                                        Log.e("###", it.message.toString())
                                    })
                        }
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {
                        val error = CropImage.getActivityResult(data).error
                        error.message?.let { Log.e("###", it) }
                    }
                }
            }
        }
    }

    private fun UploadImage(currentProfileFile: File, userId: String, upload_type: String) {
        val map = HashMap<String, RequestBody>()
        map["user_id"] = RequestBody.create(MediaType.parse("text/plain"), userId)
        map["upload_type"] = RequestBody.create(MediaType.parse("text/plain"), upload_type)

        val body = MultipartBody.Part.createFormData(
            "image", currentProfileFile.name, RequestBody.create(
                MediaType.parse("image/*"), currentProfileFile
            )
        )

        imageupload = ApiClient.apiClient?.create(Apis::class.java)?.uploadImage(map, body)
        imageupload!!.enqueue(object : Callback<ResponseImageUpload?> {
            override fun onResponse(
                call: Call<ResponseImageUpload?>,
                response: Response<ResponseImageUpload?>
            ) {
                ProgressDialogUtils.getInstance().hideProgress()
                if (response.isSuccessful && response.body() != null) {
                    imageurl = response.body()!!.data.image
                }
            }

            override fun onFailure(call: Call<ResponseImageUpload?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@EditProfileActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onBackPressed() {
        startActivity(Intent(this, ProfileActivity::class.java))
        finish()
    }

}