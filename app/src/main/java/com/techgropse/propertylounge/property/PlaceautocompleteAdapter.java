package com.techgropse.propertylounge.property;

import android.content.Context;
import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.maps.model.LatLngBounds;
import com.techgropse.propertylounge.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Sadhana on 23-08-2018.
 */

public class PlaceautocompleteAdapter extends RecyclerView.Adapter<PlaceautocompleteAdapter.PlaceViewHolder> implements Filterable {

    public interface PlaceautoCompleteInterface{
        public void onPlaceClick(ArrayList<PlaceAuto> mResultList, int position);
    }

    Context mContext;
    PlaceautoCompleteInterface mListener;
    private static final String TAG = "PlaceAutocompleteAdapter";
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    static ArrayList<PlaceAuto> mResultList;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyD_FhPR4co-Efch7TdEyoNeaHLc9BFqUBw";
    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds mBounds;
    private int layout;
    private AutocompleteFilter mPlaceFilter;

    public PlaceautocompleteAdapter(Context context, int resource, AutocompleteFilter filter){
        this.mContext = context;
        layout = resource;
        mPlaceFilter = filter;
        this.mListener = (PlaceautoCompleteInterface)mContext;
    }
    /*
    Clear List items
     */
    public void clearList(){
        if(mResultList!=null && mResultList.size()>0){
            mResultList.clear();
        }
    }
    /**
     * Sets the bounds for all subsequent queries.
     */

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    Log.e("reach","yes");
                    mResultList = autocomplete(constraint.toString());
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }

                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    Log.e("gain","yes");
                    notifyDataSetChanged();
                    // SearchActivity.text.setVisibility(View.GONE);
                }
                else {
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                    //  SearchActivity.text.setVisibility(View.VISIBLE);
                    // SearchActivity.mRecyclerView.setVisibility(View.GONE);
                }
            }
        };
        return filter;
    }

    public ArrayList autocompleteNearBy(CharSequence input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(String.valueOf(input), "utf8"));
            sb.append("&sensor=false");
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {

            return resultList;
        } catch (IOException e) {

            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                //resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                resultList.add(new PlaceAuto(predsJsonArray.getJSONObject(i).getString("description")));

            }
        } catch (JSONException e) {

        }
        return resultList;
    }

    public ArrayList autocomplete(CharSequence input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(String.valueOf(input), "utf8"));
            sb.append("&sensor=false");
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {

            return resultList;
        } catch (IOException e) {

            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                //resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                resultList.add(new PlaceAuto(predsJsonArray.getJSONObject(i).getString("description")));

            }
        } catch (JSONException e) {

        }
        return resultList;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(layout, viewGroup, false);
        PlaceViewHolder mPredictionHolder = new PlaceViewHolder(convertView);
        return mPredictionHolder;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder mPredictionHolder, final int i) {
        Log.e("size",""+mResultList.size());
        mPredictionHolder.mAddress.setText(mResultList.get(i).description);
    }

    @Override
    public int getItemCount() {
        if(mResultList != null)
            return mResultList.size();
        else
            return 0;
    }

    public static PlaceAuto getItem(int position) {
        return mResultList.get(position);
    }

    /*
    View Holder For Trip History
     */

    public class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // CardView mCardView;
        public RelativeLayout mParentLayout;
        public TextView mAddress;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            mAddress=(TextView) itemView.findViewById(R.id.textaddress);
            mAddress.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onPlaceClick(mResultList,getPosition());
        }
    }

    /**
     * Holder for Places Geo Data Autocomplete API results.
     */
    public class PlaceAuto {

        public CharSequence description;

        PlaceAuto(String description) {
            this.description = description;
        }


        @Override
        public String toString() {
            return description.toString();
        }
    }
}

