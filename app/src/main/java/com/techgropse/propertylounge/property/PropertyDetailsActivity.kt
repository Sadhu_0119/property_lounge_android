package com.techgropse.propertylounge.property

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterDetailsFacilities
import com.techgropse.propertylounge.model.ResponseHomeProperty
import kotlinx.android.synthetic.main.activity_property_details.*
import kotlinx.android.synthetic.main.activity_property_details.tvprice
import kotlinx.android.synthetic.main.activity_property_details.tvpropertyType
import kotlinx.android.synthetic.main.card_home_properties.*
import java.util.*


@Suppress("DEPRECATED_IDENTITY_EQUALS")
class PropertyDetailsActivity : AppCompatActivity(), OnMapReadyCallback,
    LocationListener,GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener{

    var mainlist= ArrayList<ResponseHomeProperty.Data>()
    var featureLst= ArrayList<ResponseHomeProperty.Data.Feature>()
    var featurelist= ArrayList<ResponseHomeProperty.Data.Feature>()
    var pos=-1
    private var mMap: GoogleMap? = null
    var mLastLocation: Location? = null
    var mCurrLocationMarker: Marker? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var mLocationRequest: LocationRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_details)

        iniT()
        arrowback.setOnClickListener {
            onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun iniT() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?

        mapFragment!!.getMapAsync(this)

        pos=intent.getIntExtra("pos",0)
        mainlist = intent.extras!!.getParcelableArrayList<ResponseHomeProperty.Data>("list") as ArrayList<ResponseHomeProperty.Data>
        featureLst = intent.extras!!.getParcelableArrayList<ResponseHomeProperty.Data.Feature>("listFeature") as ArrayList<ResponseHomeProperty.Data.Feature>

        if (!mainlist[pos].cover_image.isNullOrEmpty()) {
            Picasso.with(this).load(mainlist[pos].cover_image).into(rlimage)
        }

        if (mainlist[pos].available_id=="1"){
            tvpropertyType.text="Sell"
        }
        else{
            tvpropertyType.text="Rent"
        }

        tvname.text=mainlist[pos].name
        tvprice.text=mainlist[pos].price+" SAR"
        tvdesc.text=mainlist[pos].description
        tvagentname.text=mainlist[pos].agent_name
        tvagentnamee.text=mainlist[pos].agent_name
        tvAddress.text=mainlist[pos].location
        if (!mainlist[pos].no_of_property.isNullOrEmpty()){
            tvproCount.text=mainlist[pos].no_of_property+" properties"
        }
        else{
            tvproCount.text="0"+" properties"
        }

        for (j in 0 until featureLst.size) {
            if (featureLst[j].facilities_value_id=="0"||featureLst[j].facilities_value_id==""||featureLst[j].facilities_value_id==null){
                featurelist.remove(featureLst[j])
            }
            else{
                featurelist.add(featureLst[j])
            }
        }

        if (featurelist.size==0){
            tvNofeatures.visibility=View.VISIBLE
            rvfacilitiesList.visibility=View.GONE
        }
        else{
            tvNofeatures.visibility=View.GONE
            rvfacilitiesList.visibility=View.VISIBLE
            val adapter= AdapterDetailsFacilities(featurelist)
            rvfacilitiesList.adapter=adapter
        }
    }


    override fun onMapReady(googleMap:GoogleMap) {
        mMap = googleMap
        if (mCurrLocationMarker != null)
        {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        val latLng = LatLng(mainlist[pos].latitude.toDouble(), mainlist[pos].longitude.toDouble())
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title("Current Position")
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
        mCurrLocationMarker = mMap!!.addMarker(markerOptions)
        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11F))
        //stop location updates
        if (mGoogleApiClient != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }
    @Synchronized protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API).build()
        mGoogleApiClient!!.connect()
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if ((ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED))
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
        }
    }


    override fun onConnectionSuspended(i:Int) {
    }
    override fun onLocationChanged(location:Location) {
        mLastLocation = location
        if (mCurrLocationMarker != null)
        {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        val latLng = LatLng(location.latitude, location.longitude)
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title("Current Position")
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        mCurrLocationMarker = mMap!!.addMarker(markerOptions)
        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(11F))
        //stop location updates
        if (mGoogleApiClient != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
    }


}