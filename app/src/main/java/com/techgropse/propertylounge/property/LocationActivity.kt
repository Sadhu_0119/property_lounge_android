package com.techgropse.propertylounge.property

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterNearByplaces
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.model.location.ModelPlacesNearBy
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_location.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class LocationActivity : BaseActivity(),PlaceautocompleteAdapter.PlaceautoCompleteInterface  {
    private val API_KEY = "AIzaSyD_FhPR4co-Efch7TdEyoNeaHLc9BFqUBw"
    var mPref: SharedPreferenceUtils?=null
    var longi: String = ""
    var location: String = ""
    var lat: String = ""
    var listPlaces = ArrayList<ModelPlacesNearBy.Result>()
    var activity: Activity?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        activity=this
        mPref= SharedPreferenceUtils(this)
        lat = mPref!!.getString(Constants.LATITUDE, "")!!
        longi =  mPref!!.getString(Constants.LONGITUDE, "")!!
        location = "$lat,$longi"

        iniT()
        GetNearByPlacesAPi()

    }

    private var mLocAdapter: PlaceautocompleteAdapter? = null
    private fun iniT() {
        mLocAdapter = PlaceautocompleteAdapter(this@LocationActivity, R.layout.item_list, null)
        mLocAdapter!!.filter

        val verticalLayoutmanager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerview_location.layoutManager = verticalLayoutmanager
        recyclerview_location.adapter = mLocAdapter

        edt_location.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                recyclerview_location.visibility = View.VISIBLE
                if (count > 0) {
                    //mClear.setVisibility(View.VISIBLE);
                    if (mLocAdapter != null) {
                        recyclerview_nearby_location.visibility = View.GONE
                        recyclerview_location.adapter = mLocAdapter
                    }
                } else {
                    //mClear.setVisibility(View.GONE);
                }
                if (s.toString() != "") {
                    mLocAdapter!!.filter.filter(s.toString())
                    // Toast.makeText(mContext, "No Results Found", Toast.LENGTH_SHORT).show();
                } else {
                    // Toast.makeText(mContext, "No BeanLogin Found", Toast.LENGTH_SHORT).show();
                    Log.e("", "NOT CONNECTED")
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
    }

    /****************************** Get near by places api *********************************/
    private fun GetNearByPlacesAPi() {
        val apiService = ApiClient.apiClient!!.create(Apis::class.java)
        val call: Call<ModelPlacesNearBy>
        call = apiService.GetNearByPlaces(location, "1000", API_KEY)
        call.enqueue(object : Callback<ModelPlacesNearBy> {

            override fun onResponse(call: Call<ModelPlacesNearBy>, response: Response<ModelPlacesNearBy>) =
                if (response.isSuccessful) {
                    val listplaces: ModelPlacesNearBy? = response.body()
                    listPlaces = listplaces!!.results as ArrayList<ModelPlacesNearBy.Result>

                    recyclerview_nearby_location.layoutManager = LinearLayoutManager(this@LocationActivity, LinearLayoutManager.VERTICAL, false)
                    recyclerview_nearby_location.adapter = AdapterNearByplaces(listPlaces, this@LocationActivity,activity)
                    recyclerview_nearby_location.isNestedScrollingEnabled=false

                } else if (response.code() == 401) {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    val jsnonobj = jObjError.getString("message")
                    Toast.makeText(applicationContext, jsnonobj, Toast.LENGTH_LONG).show()
                } else if (response.code() == 403) {
                    val jObjError = JSONObject(response.errorBody()?.string())
                    val jsnonobj = jObjError.getString("message")
                    Toast.makeText(applicationContext, jsnonobj, Toast.LENGTH_LONG).show()
                } else {
                    try {
                        val jObjError = JSONObject(response.errorBody()?.string())
                        val jsnonobj = jObjError.getString("message")
                        Toast.makeText(applicationContext, jsnonobj, Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        Toast.makeText(
                            applicationContext, e.message, Toast.LENGTH_LONG
                        ).show()
                    }
                }

            override fun onFailure(call: retrofit2.Call<ModelPlacesNearBy>, t: Throwable) {
                when (t) {
                    is ConnectException -> Toast.makeText(
                        applicationContext, "Network Error",
                        Toast.LENGTH_SHORT
                    ).show()
                    is SocketTimeoutException -> Toast.makeText(
                        applicationContext, "Connection Lost",
                        Toast.LENGTH_SHORT
                    ).show()
                    is UnknownHostException -> Toast.makeText(
                        applicationContext, "Server Error",
                        Toast.LENGTH_SHORT
                    ).show()
                    is InternalError -> Toast.makeText(
                        applicationContext, "Server Error",
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> Toast.makeText(
                        applicationContext, "Server Error",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }

    override fun onPlaceClick(
        mResultList: java.util.ArrayList<PlaceautocompleteAdapter.PlaceAuto>?,
        position: Int
    ) {
        hideKeyboard(this)
        intent.putExtra("screen","map")
        intent.putExtra("value", mResultList?.get(position)!!.description)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onBackPressed() {
        hideKeyboard(this)
        intent.putExtra("screen","map")
        intent.putExtra("value", "")
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}