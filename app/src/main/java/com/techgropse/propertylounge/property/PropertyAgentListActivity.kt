package com.techgropse.propertylounge.property

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterPropertyAgentList
import com.techgropse.propertylounge.model.ResponseAgentList
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import kotlinx.android.synthetic.main.activity_property_agent_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PropertyAgentListActivity : AppCompatActivity() {
    var context:Context?=null
    var agentRequest:Call<ResponseAgentList>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_agent_list)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniT() {
        context=this
        ///api  to get agent list
        ProgressDialogUtils.getInstance().showProgress(this,false)
        GetAgentListAPI()
    }

    ///api  to get agent list
    private fun GetAgentListAPI() {
        agentRequest = ApiClient.apiClient!!.create(Apis::class.java).getAgentList()
        agentRequest!!.enqueue(object : Callback<ResponseAgentList?> {
            override fun onResponse(
                call: Call<ResponseAgentList?>,
                response: Response<ResponseAgentList?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    val list=response.body()!!.data.agent_list
                    val adapter1= AdapterPropertyAgentList(context!!,list)
                    rvPropertyListall.adapter = adapter1
                }

            }
            override fun onFailure(call: Call<ResponseAgentList?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@PropertyAgentListActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}