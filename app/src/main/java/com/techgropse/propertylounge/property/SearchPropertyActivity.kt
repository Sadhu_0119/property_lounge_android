package com.techgropse.propertylounge.property

import android.content.Intent
import android.location.Address
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.gms.vision.barcode.Barcode
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.model.ResponseHomeProperty
import com.techgropse.propertylounge.model.ResponseOTPValidate
import com.techgropse.propertylounge.model.ResponseSearchSelf
import com.techgropse.propertylounge.search.SearchActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.isValidPassword
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_search_property.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPropertyActivity : BaseActivity() {
    var available_id = ""   ////available_id= (1=sell,2=rent)
    var type = ""
    val RC_LOCATION = 3001
    var strLatitude = ""
    var strLongitude = ""
    var propertyRequest: Call<ResponseHomeProperty>? = null
    var list: ArrayList<ResponseHomeProperty.Data>? = null
    var filterlist: ArrayList<String>? = null
    var price = ""
    var flag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_property)
        filterlist = ArrayList()

        iniT()
        iniTControl()
    }

    private fun iniTControl() {
        ivBack.setOnClickListener {
            onBackPressed()
        }

        tvchange.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            startActivityForResult(intent, RC_LOCATION)
        }

        edadress.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            startActivityForResult(intent, RC_LOCATION)
        }

        tvresetsearch.setOnClickListener {
            strLatitude = ""
            strLongitude = ""
            available_id = ""
            filterlist = ArrayList()
            type = ""
            edadress.text = "Choose Address"
            if (edadress.text.toString() == "Choose Address") {
                tvchange.visibility = View.GONE
            } else {
                tvchange.visibility = View.VISIBLE
            }

            edminprice.setText("")
            edmaxprice.setText("")
            edminprice.hint = "Min price"
            edmaxprice.hint = "Max price"
            tvall.setBackgroundResource(R.drawable.rectlightblue)
            tvall.setTextColor(resources.getColor(R.color.colorPrimary))
            tvBuy.setBackgroundResource(R.drawable.rectgrey)
            tvBuy.setTextColor(resources.getColor(R.color.colorGray))
            tvRent.setBackgroundResource(R.drawable.rectgrey)
            tvRent.setTextColor(resources.getColor(R.color.colorGray))

            tvallProperty.setBackgroundResource(R.drawable.rectlightblue)
            tvallProperty.setTextColor(resources.getColor(R.color.colorPrimary))
            tvfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvfurnish.setTextColor(resources.getColor(R.color.colorGray))
            tvunfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvunfurnish.setTextColor(resources.getColor(R.color.colorGray))
        }

        tvall.setOnClickListener {
            if (filterlist!!.size > 0) {
                filterlist!!.remove("Sell")
                filterlist!!.remove("Rent")
            }
            available_id = ""
            filterlist!!.add("All")

            tvall.setBackgroundResource(R.drawable.rectlightblue)
            tvall.setTextColor(resources.getColor(R.color.colorPrimary))
            tvBuy.setBackgroundResource(R.drawable.rectgrey)
            tvBuy.setTextColor(resources.getColor(R.color.colorGray))
            tvRent.setBackgroundResource(R.drawable.rectgrey)
            tvRent.setTextColor(resources.getColor(R.color.colorGray))

        }

        tvBuy.setOnClickListener {
            if (filterlist!!.size > 0) {
                filterlist!!.remove("Rent")
                filterlist!!.remove("All")
            }
            filterlist!!.add("Sell")

            tvall.setBackgroundResource(R.drawable.rectgrey)
            tvall.setTextColor(resources.getColor(R.color.colorGray))
            tvBuy.setBackgroundResource(R.drawable.rectlightblue)
            tvBuy.setTextColor(resources.getColor(R.color.colorPrimary))
            tvRent.setBackgroundResource(R.drawable.rectgrey)
            tvRent.setTextColor(resources.getColor(R.color.colorGray))
            available_id = "1"
        }

        tvRent.setOnClickListener {
            available_id = "2"
            if (filterlist!!.size > 0) {
                filterlist!!.remove("All")
                filterlist!!.remove("Sell")
            }
            filterlist!!.add("Rent")


            tvall.setBackgroundResource(R.drawable.rectgrey)
            tvall.setTextColor(resources.getColor(R.color.colorGray))
            tvRent.setBackgroundResource(R.drawable.rectlightblue)
            tvRent.setTextColor(resources.getColor(R.color.colorPrimary))
            tvBuy.setBackgroundResource(R.drawable.rectgrey)
            tvBuy.setTextColor(resources.getColor(R.color.colorGray))

        }

        tvallProperty.setOnClickListener {
            type = ""
            if (filterlist!!.size > 0) {
                filterlist!!.remove("Furnished")
                filterlist!!.remove("UnFurnished")
            }
            filterlist!!.add("All Property")

            tvallProperty.setBackgroundResource(R.drawable.rectlightblue)
            tvallProperty.setTextColor(resources.getColor(R.color.colorPrimary))
            tvfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvfurnish.setTextColor(resources.getColor(R.color.colorGray))
            tvunfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvunfurnish.setTextColor(resources.getColor(R.color.colorGray))
        }

        tvfurnish.setOnClickListener {
            type = "1"
            if (filterlist!!.size > 0) {
                filterlist!!.remove("All Property")
                filterlist!!.remove("UnFurnished")
            }
            filterlist!!.add("Furnished")
            tvfurnish.setBackgroundResource(R.drawable.rectlightblue)
            tvfurnish.setTextColor(resources.getColor(R.color.colorPrimary))
            tvallProperty.setBackgroundResource(R.drawable.rectgrey)
            tvallProperty.setTextColor(resources.getColor(R.color.colorGray))
            tvunfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvunfurnish.setTextColor(resources.getColor(R.color.colorGray))

        }

        tvunfurnish.setOnClickListener {
            type = "2"
            if (filterlist!!.size > 0) {
                filterlist!!.remove("All Property")
                filterlist!!.remove("Furnished")
            }

            filterlist!!.add("UnFurnished")
            tvunfurnish.setBackgroundResource(R.drawable.rectlightblue)
            tvunfurnish.setTextColor(resources.getColor(R.color.colorPrimary))
            tvallProperty.setBackgroundResource(R.drawable.rectgrey)
            tvallProperty.setTextColor(resources.getColor(R.color.colorGray))
            tvfurnish.setBackgroundResource(R.drawable.rectgrey)
            tvfurnish.setTextColor(resources.getColor(R.color.colorGray))

        }

        tvSearchproperty.setOnClickListener {
            val minprice = edminprice.text.toString().trim()
            val maxprice = edmaxprice.text.toString().trim()
            if (edadress.text.toString() == "Choose Address") {
                Toast.makeText(this, "Please select address", Toast.LENGTH_SHORT).show()
            } else if (minprice.isEmpty()) {
                Toast.makeText(this, "Please enter min price", Toast.LENGTH_SHORT).show()
            } else if (maxprice.isEmpty()) {
                Toast.makeText(this, "Please enter max price", Toast.LENGTH_SHORT).show()
            } else if (maxprice.toInt() < minprice.toInt()) {
                Toast.makeText(
                    this,
                    "Max price should be greater than min price",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (minprice.toInt() > maxprice.toInt()) {
                Toast.makeText(
                    this,
                    "Min price cannot be greater than max price",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                //search property api
                price = "$minprice-$maxprice"
                ProgressDialogUtils.getInstance().showProgress(this, false)
                SearchpropertyAPi(minprice, maxprice, edadress.text.toString())
            }
        }
    }

    //search property api
    private fun SearchpropertyAPi(minprice: String, maxprice: String, toString: String) {
        propertyRequest = ApiClient.apiClient!!.create(Apis::class.java)
            .searchpropertyList(strLatitude, strLongitude, available_id, type, minprice, maxprice)
        propertyRequest!!.enqueue(object : Callback<ResponseHomeProperty?> {
            override fun onResponse(
                call: Call<ResponseHomeProperty?>,
                response: Response<ResponseHomeProperty?>
            ) {
                if (response.isSuccessful) {
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code == 200) {
                        list = response.body()!!.data
                    } else {
                        list = ArrayList()
                    }

                    flag = true
                    val intent = Intent(this@SearchPropertyActivity, SearchActivity::class.java)
                    intent.putParcelableArrayListExtra("list", list)
                    if (filterlist!!.size==0){
                        val list=ArrayList<String>()
                        list.add("All")
                        list.add("All Property")
                        intent.putExtra("filterlist", list)
                    }
                    else{
                        intent.putExtra("filterlist", filterlist)
                    }
                    intent.putExtra("price", price)
                    startActivity(intent)

                }

            }

            override fun onFailure(call: Call<ResponseHomeProperty?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@SearchPropertyActivity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun iniT() {
    }

    private var value: String? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_LOCATION -> {
                value = data!!.getStringExtra("value")
                if (value == "") {

                } else {
                    tvchange.visibility = View.VISIBLE
                    edadress.text = value
                    getLocationFromAddress(value!!)
                }
            }
        }
    }

    private fun getLocationFromAddress(value: String): Barcode.GeoPoint? {
        val coder = Geocoder(this)
        val address: List<Address>
        var p1: Barcode.GeoPoint? = null
        try {
            address = coder.getFromLocationName(value, 5)
            if (address == null) {
                return null
            }
            val location = address.get(0)
            strLongitude = location.longitude.toString()
            strLatitude = location.latitude.toString()

            location.latitude
            location.longitude
            p1 = Barcode.GeoPoint(
                (location.latitude * 1E6) as Double,
                (location.longitude * 1E6) as Double
            )
        } catch (e: IndexOutOfBoundsException) {

        }
        return p1
    }

    override fun onResume() {
        super.onResume()
    }
}