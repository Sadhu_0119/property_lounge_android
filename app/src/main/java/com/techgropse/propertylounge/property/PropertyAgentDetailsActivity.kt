package com.techgropse.propertylounge.property

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.adapter.AdapterPropertiesHomeList
import kotlinx.android.synthetic.main.activity_property_agent_details.*

class PropertyAgentDetailsActivity : AppCompatActivity() {
    var context: Context?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_agent_details)
        iniT()
        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBack.setOnClickListener {
            onBackPressed()
        }

        ivFilter.setOnClickListener {
            startActivity(Intent(this,SearchPropertyActivity::class.java))
        }
    }

    private fun iniT() {
        context=this
        val adapter1= AdapterPropertiesHomeList(context!!)
        rvPropertyListall.adapter=adapter1
    }
}