package com.techgropse.propertylounge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import kotlinx.android.synthetic.main.cardsearch.view.*
import java.util.ArrayList

class AdapterSearch(val filter: ArrayList<String>?, context: Context,val listnerrr:RemoveListener) :
    RecyclerView.Adapter<AdapterSearch.ViewHolder>() {

    var listener:RemoveListener?=null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        listener=listnerrr
        holder.itemView.tvFiltername.text=filter!![position]

        holder.itemView.cross.setOnClickListener {
            listener!!.remove(position)
        }
    }
    override fun getItemCount(): Int = filter!!.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.cardsearch,
                    parent,
                    false
                )
        )

    interface RemoveListener{
        fun remove(pos:Int)
    }
}