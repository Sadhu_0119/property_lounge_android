package com.techgropse.propertylounge.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.location.ModelPlacesNearBy
import com.techgropse.propertylounge.property.LocationActivity
import kotlinx.android.synthetic.main.item_list.view.*

class AdapterNearByplaces(
    private var lists: ArrayList<ModelPlacesNearBy.Result>,
    var context: Context,
    var activity: Activity?
) : RecyclerView.Adapter<AdapterNearByplaces.ViewHolder>() {


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.ll_places.setOnClickListener {
            val inten = (activity as LocationActivity).intent
            inten.putExtra("value", lists[position].name + " " + lists[position].vicinity)
            inten.putExtra("screen", "map")
            (activity)!!.setResult(Activity.RESULT_OK, inten)
            activity!!.finish()
        }

        holder.itemView.textaddress.text=lists[position].name + " " + lists[position].vicinity
    }

    override fun getItemCount(): Int = lists.size
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false))
}