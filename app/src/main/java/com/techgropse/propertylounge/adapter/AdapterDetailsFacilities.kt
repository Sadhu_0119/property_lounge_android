package com.techgropse.propertylounge.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseAgentList
import com.techgropse.propertylounge.model.ResponseHomeProperty
import kotlinx.android.synthetic.main.card_agents.view.*
import kotlinx.android.synthetic.main.card_facilities_details.view.*

class AdapterDetailsFacilities (val list: List<ResponseHomeProperty.Data.Feature>) :
    RecyclerView.Adapter<AdapterDetailsFacilities.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.name.text=list[position].name
        holder.itemView.tvCount.text=list[position].facilities_value_id

    }

    override fun getItemCount(): Int = list.size
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_facilities_details, parent,
                false
            )
        )
}