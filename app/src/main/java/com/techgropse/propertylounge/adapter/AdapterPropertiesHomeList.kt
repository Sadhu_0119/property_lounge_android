package com.techgropse.propertylounge.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.home.HomeActivity
import com.techgropse.propertylounge.property.PropertyAgentDetailsActivity
import com.techgropse.propertylounge.property.PropertyDetailsActivity

class AdapterPropertiesHomeList(val context: Context) :
    RecyclerView.Adapter<AdapterPropertiesHomeList.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            Toast.makeText(context,"Under Development", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int = 4
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.card_home_properties,
                    parent,
                    false
                )
        )
}