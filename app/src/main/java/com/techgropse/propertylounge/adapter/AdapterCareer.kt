package com.techgropse.propertylounge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseCareerList
import com.techgropse.propertylounge.new_career.CareerJobApplyActivity
import kotlinx.android.synthetic.main.act_news_details_user.*
import kotlinx.android.synthetic.main.card_career.view.*
import java.text.SimpleDateFormat
import java.util.*

class AdapterCareer(
    val context: Context,
    val list: List<ResponseCareerList.Data.Career>
) :
    RecyclerView.Adapter<AdapterCareer.ViewHolder>() {

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context, CareerJobApplyActivity::class.java))
        }
        holder.itemView.tvName.text=list[position].title
        holder.itemView.tvdesc.text=list[position].description
        holder.itemView.tvpos.text=list[position].no_of_position+" Position"
        var str=list[position].created_at
        val separated = str.split(" ")
        var date=separated[0]
        var splitdate=date.split("-")
        var finaldate=splitdate[2]+"/"+splitdate[1]+"/"+splitdate[0]
        holder.itemView.tvdate.text=finaldate
    }

    override fun getItemCount(): Int =list.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_career, parent, false)
        )

}