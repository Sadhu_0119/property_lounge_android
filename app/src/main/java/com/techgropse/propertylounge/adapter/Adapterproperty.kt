package com.techgropse.propertylounge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseHomeProperty
import com.techgropse.propertylounge.property.PropertyDetailsActivity
import kotlinx.android.synthetic.main.card_home_properties.view.*

class Adapterproperty(
    val context: Context,
    val list: ArrayList<ResponseHomeProperty.Data>,
    var facilitiesList: ArrayList<ResponseHomeProperty.Data.Feature>?
) :
    RecyclerView.Adapter<Adapterproperty.ViewHolder>() {


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val intent=Intent(context,PropertyDetailsActivity::class.java)
            intent.putParcelableArrayListExtra("list", list)
            intent.putParcelableArrayListExtra("listFeature", list[position].features)
            intent.putExtra("pos",position)
            context.startActivity(intent)
        }

        if (!list[position].cover_image.isNullOrEmpty()) {
            Picasso.with(context).load(list[position].cover_image).into(holder.itemView.ivCoverImage)
        }
        //1=sell ,2=rent
        if (list[position].available_id=="1"){
            holder.itemView.tvpropertyType.text="Sell"
        }
        else{
            holder.itemView.tvpropertyType.text="Rent"
        }
        holder.itemView.tvpropertyname.text=list[position].name
        holder.itemView.tvLoc.text=list[position].location
        holder.itemView.tvprice.text=list[position].price+" SAR"

        facilitiesList= ArrayList()

        for (j in 0 until list[position].features.size) {
            if (list[position].features[j].facilities_value_id=="0"||list[position].features[j].facilities_value_id==""||list[position].features[j].facilities_value_id.isNullOrEmpty()){
                facilitiesList!!.remove(list[position].features[j])
            }
            else{
                facilitiesList!!.add(list[position].features[j])
            }
        }

        val adapter1=AdapterFacilities(context,facilitiesList!!)
        holder.itemView.rvFacilities.adapter=adapter1
    }

    override fun getItemCount(): Int =list.size
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.card_home_properties,
                    parent,
                    false
                )
        )
}