package com.techgropse.propertylounge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseHomeProperty
import kotlinx.android.synthetic.main.card_facilities.view.*

class AdapterFacilities(
    val context: Context,
    val features: List<ResponseHomeProperty.Data.Feature>
) :
    RecyclerView.Adapter<AdapterFacilities.ViewHolder>() {

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val upperString: String = features[position].name.substring(0, 1).toUpperCase() + features[position].name.substring(1)
        holder.itemView.tvfacilities.text=features[position].facilities_value_id+upperString

    }

    override fun getItemCount(): Int = features.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_facilities, parent, false)
        )
}