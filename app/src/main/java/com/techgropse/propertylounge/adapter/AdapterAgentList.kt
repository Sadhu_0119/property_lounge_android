package com.techgropse.propertylounge.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseAgentList
import kotlinx.android.synthetic.main.card_agents.view.*
import kotlinx.android.synthetic.main.card_agents.view.tvName
import kotlinx.android.synthetic.main.card_home_properties.view.*
import kotlinx.android.synthetic.main.card_news.view.*

class AdapterAgentList(
    val list: List<ResponseAgentList.Data.Agent>,
    var context: Context
) :
    RecyclerView.Adapter<AdapterAgentList.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvName.text = list[position].full_name
        if (!list[position].image.isNullOrEmpty()) {
                Picasso.with(context).load(list[position].image).into(holder.itemView.image)
            }

    }

    override fun getItemCount(): Int = list.size
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.card_agents, parent,
                false
            )
        )
}