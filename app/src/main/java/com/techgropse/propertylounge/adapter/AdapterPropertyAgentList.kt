package com.techgropse.propertylounge.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseAgentList
import com.techgropse.propertylounge.property.PropertyAgentDetailsActivity
import kotlinx.android.synthetic.main.card_agents.view.*
import kotlinx.android.synthetic.main.card_property_agentlist.view.*

class AdapterPropertyAgentList(
    val context: Context,
    val list: List<ResponseAgentList.Data.Agent>
) :

RecyclerView.Adapter<AdapterPropertyAgentList.ViewHolder>() {

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context,PropertyAgentDetailsActivity::class.java))
        }

        holder.itemView.tvproname.text = list[position].full_name
        holder.itemView.tvproCount.text=list[position].no_of_property+" Properties"

            if (!list[position].image.isNullOrEmpty()) {
                Picasso.with(context).load(list[position].image).into(holder.itemView.ivPerson)
            }

    }

    override fun getItemCount(): Int = list.size
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.card_property_agentlist, parent, false
                )
        )
}