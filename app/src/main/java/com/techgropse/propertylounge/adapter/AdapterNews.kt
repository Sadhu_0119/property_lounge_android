package com.techgropse.propertylounge.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.model.ResponseNewsList
import com.techgropse.propertylounge.new_career.NewsActivity
import com.techgropse.propertylounge.new_career.NewsDetailsActivity
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.card_news.view.*
import java.text.SimpleDateFormat
import java.util.*

class AdapterNews(
    val context: Context,
    val list: List<ResponseNewsList.Data.News>
) :
    RecyclerView.Adapter<AdapterNews.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (!list[position].image.isNullOrEmpty()) {
            Picasso.with(context).load(list[position].image).into(holder.itemView.ivNews)
        }
        holder.itemView.tvHeadline.text=list[position].title
        holder.itemView.tvdesc.text=list[position].description

        var str=list[position].created_at
        val separated = str.split(" ")
        var date=separated[0]
        var splitdate=date.split("-")
        var finaldate=splitdate[2]+"/"+splitdate[1]+"/"+splitdate[0]
        holder.itemView.newsdate.text=finaldate

        holder.itemView.setOnClickListener {
            val intent=Intent(context,NewsDetailsActivity::class.java)
            intent.putExtra("newsId",list[position].news_id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int =list.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_news, parent, false))

}