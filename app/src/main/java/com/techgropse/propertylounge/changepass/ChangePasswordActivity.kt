package com.techgropse.propertylounge.changepass

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.techgropse.propertylounge.R
import com.techgropse.propertylounge.base.BaseActivity
import com.techgropse.propertylounge.model.ResponseChangePass
import com.techgropse.propertylounge.profile.ProfileActivity
import com.techgropse.propertylounge.utils.ProgressDialogUtils
import com.techgropse.propertylounge.utils.SharedPreferenceUtils
import com.techgropse.propertylounge.utils.isValidPassword
import com.techgropse.propertylounge.webservice.ApiClient
import com.techgropse.propertylounge.webservice.Apis
import com.techgropse.propertylounge.webservice.Constants
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.dialogpass.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : BaseActivity() {
    var mPref: SharedPreferenceUtils?=null
    var userId=""
    var deviceId=""
    var securityToken=""
    var resetRequest: Call<ResponseChangePass>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        mPref= SharedPreferenceUtils(this)
        userId=mPref!!.getString(Constants.USERID,"")
        deviceId=mPref!!.getString(Constants.DEVICE_ID,"")
        securityToken=mPref!!.getString(Constants.SECURITY_TOKEN,"")

        iniTcontrol()
    }

    private fun iniTcontrol() {
        ivBack.setOnClickListener {
            onBackPressed()
        }

        btnchangepassword.setOnClickListener {
            val password=ednewpass.text.toString().trim()
            val repass=edconnewpass.text.toString().trim()
            val oldpassword=edoldpass.text.toString().trim()

            if (oldpassword.isEmpty()) {
                Toast.makeText(this@ChangePasswordActivity,"Please enter old password", Toast.LENGTH_SHORT).show()
            } else if (!oldpassword.isValidPassword()) {
                Toast.makeText(this@ChangePasswordActivity,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }else if (password.isEmpty()) {
                Toast.makeText(this@ChangePasswordActivity,"Please enter new password", Toast.LENGTH_SHORT).show()
            }
            else if (!password.isValidPassword()) {
                Toast.makeText(this@ChangePasswordActivity,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }
            else if (repass.isEmpty()) {
                Toast.makeText(this@ChangePasswordActivity,"Please enter confirm new password", Toast.LENGTH_SHORT).show()
            }
            else if (!repass.isValidPassword()) {
                Toast.makeText(this@ChangePasswordActivity,resources.getString(R.string.error_valid_password), Toast.LENGTH_SHORT).show()
            }
            else if(password != repass){
                Toast.makeText(this@ChangePasswordActivity,"New password and confirm new password donot match", Toast.LENGTH_SHORT).show()
            }
            else{
                //api call to change password
                ProgressDialogUtils.getInstance().showProgress(this,false)
                ApiChangepassUser(userId,oldpassword,password)

            }
        }
    }

    ////api to change password for user
    private fun ApiChangepassUser(userId: String, oldpassword: String, password: String) {
        resetRequest = ApiClient.apiClient!!.create(Apis::class.java).changepassword(deviceId,securityToken,userId,oldpassword,password)
        resetRequest!!.enqueue(object : Callback<ResponseChangePass?> {
            override fun onResponse(
                call: Call<ResponseChangePass?>,
                response: Response<ResponseChangePass?>
            ) {
                if (response.isSuccessful){
                    ProgressDialogUtils.getInstance().hideProgress()
                    if (response.body()!!.error_code==200){
                        dialogopen()
                    }
                    else{
                        Toast.makeText(this@ChangePasswordActivity,response.body()!!.message, Toast.LENGTH_SHORT).show()
                    }

                }

            }
            override fun onFailure(call: Call<ResponseChangePass?>, t: Throwable) {
                ProgressDialogUtils.getInstance().hideProgress()
                Toast.makeText(this@ChangePasswordActivity,t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun dialogopen() {
        val dialog = Dialog( this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialogpass)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        dialog.tvOOOk.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
            finish()
        }
        dialog.show()
    }
}